<?php 
class CategoryModel extends CI_Model
{
  function __construct()
  {
    parent::__construct();
  }

  function getCategory($id)
  {
  	$this->db->where('id',$id);
  	return $this->db->get('category')->row_array();
  }
  function getAllCategory()
  {
  	return $this->db->get('category')->result_array();
  }

  function addCategory($params=array())
  {
  	$this->db->insert('category', $params);
    return $this->db->insert_id();
  }

  function updateCategory($id,$params)
  {
  	$this->db->where('id',$id);
  	return $this->db->update('category', $params);
  }
  function deleteCategory($id)
  {
    $this->db->where('id',$id);
    return $this->db->delete('category');
  }
}
 ?>