<?php 
class ProductModel extends CI_Model
{
  function __construct()
  {
    parent::__construct();
  }

  function getProduct($id)
  {
  	$this->db->where('id',$id);
  	return $this->db->get('product')->row_array();
  }
  function getAllProducts($userId)
  {
    if($userId)
    {
      $this->db->where('userId',$userId);
    }
  	return $this->db->get('product')->result_array();
  }

  function addProduct($params=array())
  {
  	$this->db->insert('product', $params);
    return $this->db->insert_id();
  }

  function updateProduct($id,$params)
  {
  	$this->db->where('id',$id);
  	return $this->db->update('product', $params);
  }
  function deleteCategory($id)
  {
    $this->db->where('id',$id);
    return $this->db->delete('product');
  }
}
 ?>