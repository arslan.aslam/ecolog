<?php
class UserModel extends CI_Model
{
  function __construct()
  {
    parent::__construct();
  }

  /*
     * Get user by id
     */
  function getUser($id)
  {
    $user = $this->db->get_where('user', array('id' => $id))->row_array();
    // unset($user['password']);
    return $user;

  }

  /*
     * Get all user count
     */
  function getAllUserCount($userType)
  {
    if($userType)
    {
      $this->db->where('userType', $userType);
    }
    $this->db->from('user');
    return $this->db->count_all_results();
  }

  /*
     * Get all user
     */
  function getAllUser($params = array())
  {
    $this->db->order_by('id', 'desc');
    $this->db->select('id,sessionId, email, firstName, lastName, status, userType,address, isAdmin, vehicleNo, passwordToken, phone, dateCreated, dateModified ');

    if (isset($params['userType']) && !empty($params['userType'])) {
      $this->db->where('userType', $params['userType']);
    }

    if (isset($params['limit']) && !empty($params['limit'])) {
      $this->db->limit($params['limit'], $params['offset']);
    }
    if (!empty($params['userIds'])) {
      $this->db->where_in('id', $params['userIds']);
    }
    if (!empty($params['sessionId'])) {
      $this->db->where('sessionId', $params['sessionId']);
    }
    if (!empty($params['createdId'])) {
      $this->db->where('createdId', $params['createdId']);
    }
    if (isset($params['status'])) {
      $this->db->where('status', $params['status']);
    }



    return $this->db->get('user')->result_array();
  }

  /*
     * function to add new user
     */
  function addUser($params)
  {
    $this->db->insert('user', $params);
    return $this->db->insert_id();
  }

  /*
     * function to update user
     */
  function updateUser($id, $params)
  {
    $this->db->where('id', $id);
    return $this->db->update('user', $params);
  }

  /*
     * function to delete user
     */
  function deleteUser($id)
  {
    return $this->db->delete('user', array('id' => $id));
  }


  /*
     * Email Verify
     */


  function isEmailExist($params = array())
  {

    $this->db->select('id, email,status,firstName, lastName');
    $this->db->from('user');
    $this->db->where('email', $params['email']);
    return $this->db->get()->row_array();
  }


  /*
     * Verify Credentials 
     */



  function validateCredential($params = array())
  {

    $this->db->select('*');
    if (isset($params['email']) && !empty($params['email'])) {
      $this->db->where('email', $params['email']);
    }
    if (isset($params['phone']) && !empty($params['phone'])) {
      $this->db->where('phone', $params['phone']);
    }

    if (isset($params['password']) && !empty($params['password'])) {
      $this->db->where('password', $params['password']);
    }

    if (isset($params['userType']) && !empty($params['userType'])) {
      $this->db->where('userType', $params['userType']);
    }

    $user = $this->db->get('user')->row_array();
    unset($user['password']);
    return $user;
  }

  // checking if user already activated his account..
  function checkAlreadyActivation($key)
  {
    $this->db->select('status');
    return $this->db->get_where('user', array('md5(email)' => $key))->row_array();
  }
  function verifyEmail($key)
  {
    $data = array('status' => '1');
    $this->db->where('md5(email)', $key);
    return $this->db->update('user', $data);
  }

  // Check token for Forget Password 
  function passwordTokenExist($token)
  {
    $this->db->where('passwordToken', $token);
    return   $this->db->get('user')->row_array();
  }


  function updatePasswordWithEmail($token, $params)
  {

    $this->db->trans_start();
    $this->db->where('md5(email)', $token);
    $this->db->update('user', $params);
    $this->db->trans_complete();

    if ($this->db->trans_status() === FALSE) {

      return false;
    }
    return true;
  }
  // Update Password through Password Token  

  function forgetPassword($token, $params)
  {

    $this->db->trans_start();
    $this->db->where('passwordToken', $token);
    $this->db->update('user', $params);
    $this->db->trans_complete();

    if ($this->db->affected_rows() == '1') {

      $token_update = array(

        'passwordToken'    =>  null,
        // 'sessionID'    =>  null,

      );
      $this->db->where('passwordToken', $token);
      $this->db->update('user', $token_update);
      return true;
    } else {
      if ($this->db->trans_status() === FALSE) {

        return false;
      }


      return true;
    }
  }


  function validateSession($userId, $sessionId)
  {


    $this->db->select('id, sessionId');
    $this->db->from('user');
    $this->db->where('id', $userId);
    $this->db->where('sessionId', $sessionId);
    $query = $this->db->get();
    $row =  $query->row();

    if ($row && !empty($row)) {
      return true;
    }

    return false;
  }



  function addInvitedUser($params)
  {
    $this->db->insert('invited_user', $params);
    return $this->db->insert_id();
  }


  function getAllInvitedUser($params = array())
  {
    $this->db->order_by('id', 'desc');
    $this->db->group_by('invitedTo');
    if (!empty($params['invitedBy'])) {

      $this->db->where('invitedBy', $params['invitedBy']);
    }
    $this->db->select('invited_user.*, user.id as userId, user.email, user.sessionId, user.firstName, user.lastName,user.userType, user.image, user.deviceToken, user.deviceToken ');
    $this->db->join('user', 'user.id = invited_user.invitedTo');
    return  $this->db->get('invited_user')->result_array();
  }


  function getAllReceiverEmail()
  {
    return $this->db->get('emails')->result_array();
  }
}
