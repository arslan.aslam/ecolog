<!DOCTYPE html>
<html lang="en">

<head>
  <title>Forgot Password</title>
  <meta charset="utf-8">
  <link rel="shortcut icon" type="image/png" href="assets/images/favicon.png" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
  <!-- font-family: 'Lato', sans-serif; -->

</head>
<style>
  body {
    font-family: Arial, Helvetica, sans-serif !important;
  }
</style>

<body>
  <div class="container " style="padding: 50px 0px; text-align: center;

	">



    <div class="row  text-center" style="
padding: 20px 0px 0px 0px !important;
background: rgb(255,255,255) !important;
width:100%;
max-width: 600px !important;
margin: -5px auto !important;
text-align: center;
box-shadow: 0px 6px 12px rgba(0,0,0,0.16);
border: 1px solid #eee;">

      <div class="text-center " style="
  width:100%;
  max-width: 600px !important;
  margin: 20px auto 0px !important;
  padding: 10px 0px;
  text-align: center;display: inline-block; ">
        <img class="" style="width: 150px;
  object-fit: contain;" src="<?php echo base_url().'assets/images/dummy/eco.png?v=1'?>">
      </div>

      <div class="col-md-12 ">
        <h4 class="" style="
  color: #74a857;
  font-size: 30px;
  line-height: 1.167;
  font-weight: 600;
  margin-top: 0;
  ">Forgot your password</h4>

      </div>



      <div class="col-md-12">
        <div>
          <p style="
    font-size: 17px;
    color: #000;
    line-height: 1.2;
    font-weight: 500;">Hello! <?php echo $name; ?>,</p>
          <p style="
        font-size: 14px;
        color: #848484;
        line-height: 1.2;
        letter-spacing: 0.03rem;">Looks like you forgot your password for Eco Care. <br>
            If this is correct, Click below to reset your password <br>
            <!-- Your OTP is:  <span style="
                 font-size: 18px;
                 color: #000;
                 line-height: 1.2;
                 letter-spacing: 0.3em;
                 font-weight: 600;">1234</span></p> -->
          </p>
          <div class="  login-form-footer" style="margin-top:30px;
          margin-bottom:30px;">
            <a href="<?php echo base_url() ?>update-password/<?php echo $token; ?>" class="home-form-btn " style="    font-size: 15px;
          color: #fff !important;
          background:#9d1c25;  /* fallback for old browsers */
          line-height: 1.5 !important;
          font-weight: 700;
          min-width: 250px;
          height: 40px;
          box-shadow: 0px 4px 16px rgba(0,0,0,0.16);
          text-align: center;
          border-radius: 6px;
          text-decoration: none;
          padding: 10px 20px;"> Reset my password</a><br><br>

          </div>



          <div>


            <p style="
 font-size: 14px;
 margin-top: 40px;
 color: #aaa9a9;
 line-height: 0.2;
 letter-spacing: 0.04rem;">If you did not forgot your password, Please ignore this email.

            </p>
            <p style="
    font-size: 14px;
    color: #848484;
    line-height: 1.2;margin-top: 0;margin-bottom: 0;">Thanks</p>
            <p style="
    font-size: 16px;
    color: #000;
    line-height: 1.6;
    font-weight: 600;
    letter-spacing: .04rem; margin-top: 2px;margin-bottom:0;">Eco Log Team</p>

            <p style="
    font-size: 14px;
    color: #848484; margin-top: 0;
    line-height: 1.2;">This is an automated message, Please do not reply.</p>
          </div>
          <div style="
  padding: 10px 0px;
  background-repeat: no-repeat;
  background-size: cover;
  background:#9d1c25;
  box-shadow: 0px -2px 8px 0px rgba(0,0,0,.3);">



            <div>

              <div>
                <p style="color: #fff;
         font-size: 14px;
         font-weight: 500;
         letter-spacing: 0.03em;
         padding: 15px 0px;
         /* margin-top: 8px; */
         margin: 0;">© <?php echo date('Y'); ?> Eco Log All Rights Reserved</p>
              </div>
            </div>

          </div>
        </div>




      </div>

    </div>
  </div>











  </div>
</body>

</html>


<!-- <!DOCTYPE html>
<html lang="en">
<head>
	<title>NoHalt</title>
	<meta charset="utf-8">	
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<style >
  body{
   font-family: Arial, Helvetica, sans-serif !important; 
 }

</style>
<body>  
	<div style="background-color:#f3f3f3;">

		<div style="padding: 10px!important; background:#fff !important; width:600px !important; margin:0px auto !important; text-align: center">

			<div style="padding-bottom: 20px;">
				<img style="width: 200px;
				 background: -webkit-linear-gradient(45deg, #F59E52, #482C6D);
                background: linear-gradient(45deg, #F59E52, #482C6D);
                background: -ms-linear-gradient(45deg, #F59E52, #482C6D);
                background: -moz-linear-gradient(45deg, #F59E52, #482C6D);
                background: -o-linear-gradient(45deg, #F59E52, #482C6D);
                padding: 10px;
                border-radius: 10px;
                -webkit-box-shadow: 0px 2px 10px -1px rgba(130,126,130,1);
-moz-box-shadow: 0px 2px 10px -1px rgba(130,126,130,1);
box-shadow: 0px 2px 10px -1px rgba(130,126,130,1);
				" 
				
				src="<?php echo base_url('assets/images/logo1.png'); ?>">

			</div>
			<div>
				<h4 style=" color: #E79247; font-size: 32px; line-height: 1.167; font-weight: 600; ">Forget Your Password</h4>
			</div>
			<div>
				<div>
					<p style="font-size:16px; margin-bottom:20px;">Looks like you forget you password for NoHalt. <br>
						If this is correct, click below to reset your password.
					</p>

					<div style="margin-top:30px; margin-bottom:30px;">

						<a href="<?php echo base_url() ?>update-password/<?php echo $token; ?>" style=" font-size: 17px; color: #E79247; background-color: #fff; line-height: 1.5; font-weight: 700; min-width: 250px; height: 40px; border: 2px solid #E79247; text-align: center; border-radius: 10px; text-decoration: none; text-transform: uppercase; padding: 17px;"> Reset your password
						</a>
						<br><br>

					</div>
					<p style="font-size:15px; ">or copy and pase this link into your browser:</p>
					<p style="    font-size: 12px; "><?php echo base_url() ?>User/updatePassword/<?php echo $token; ?></p>
					<p style="font-size:15px; "> If you did not forgot your password, please ignore this email.</p>
					<p style="font-size: 22px;">Thanks</p>
					<p style="font-size:15px; ">NoHalt Team</p>
				</div>
			<p><a style=" color: #fea034; font-weight: 600; cursor:pointer;">Privacy Policy</a>
					&nbsp;|&nbsp; 
					<a style=" color: #fea034; font-weight: 600; cursor:pointer;">About</a> &nbsp;|&nbsp; <a style=" color: #fea034; font-weight: 600; cursor:pointer;">Contact</a> &nbsp;|&nbsp; <a style=" color: #fea034; font-weight: 600; cursor:pointer;">Legal Notice</a>
				</p>
				<p style=" font-size:14px;color: #848484;font-weight:600;">&copy; NoHalt</p>



			</div>

		</div>
	</div>


</body> 
</html>


 -->