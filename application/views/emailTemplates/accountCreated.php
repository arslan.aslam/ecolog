<!DOCTYPE html>
<html lang="en">

<head>
	<title>Eco Log</title>
	<meta charset="utf-8">
	<link rel="shortcut icon" type="image/png" href="assets/images/favicon.png" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
	<!-- font-family: 'Lato', sans-serif; -->

</head>
<style>
	body {
		font-family: Arial, Helvetica, sans-serif !important;
	}
</style>

<body>
	<div class="container " style="background-color:#f3f3f3;">

		<div class="row  text-center" style="padding: 10px!important;
		background:#fff !important;
		width:600px !important;

		margin:1px auto !important; text-align: center">

			<div class="col-md-12 ">
				<div class="text-center " style="
  width:100%;
  max-width: 600px !important;
  margin: 20px auto 0px !important;
  padding: 10px 0px;
  text-align: center;display: inline-block; ">
					<img class="" style="width: 150px;
  object-fit: contain;" src="<?php echo base_url().'assets/images/dummy/eco.png?v=1' ?>">
				</div>
				<h4 class="" style="
			color: #9d1c25;
			font-size: 32px;
			line-height: 1.167;
			font-weight: 600;
			">Account Created</h4>
			</div>



			<div class="col-md-12">
				<div>



					<p style="
				font-size: 17px;
				color: #848484;
				line-height: 1.2;">Hello! <?php echo $firstName.' '.$lastName; ?></p>

					<p style="
				font-size: 14px;
				color: #848484;
				line-height: 1.2;">Welcome to Eco Log. An invitation has been sent to you as <?php if($userType == 2){echo "Manager";}
				elseif($userType == 3){echo "Driver";}
				elseif($userType == 4){echo "Warehouse";}
				elseif($userType == 5){echo "Branch";}?> <br>
						Your email is: <?php echo  $email ?><br>
						<?php echo !empty($password) ? 'Your password is: ' . $password : '' ?><br>
					</p>
					<div>
						<p style="
				font-size: 14px;
				color: #848484;
				line-height: 1.2;">For additional support, please do not hesitate to contact us.For additional support, please do not hesitate to contact us.</p>
						<p style="
				font-size: 14px;
				color: #848484;
				line-height: 0.2;">Thanks</p>

						<p style="
				font-size: 14px;
				color: #848484;
				line-height: 0.2;">Eco Log.
						</p>

						<p> *This is an automated message, please do not reply. </p>
					</div>
				</div>

				<div style="
  padding: 10px 0px;
  background-repeat: no-repeat;
  background-size: cover;
  background:#9d1c25;
  box-shadow: 0px -2px 8px 0px rgba(0,0,0,.3);">



					<div>

						<div>
							<p style="color: #fff;
         font-size: 14px;
         font-weight: 500;
         letter-spacing: 0.03em;
         padding: 15px 0px;
         /* margin-top: 8px; */
         margin: 0;">© <?php echo date('Y'); ?> Eco Log All Rights Reserved</p>
						</div>
					</div>

				</div>


			</div>

		</div>
	</div>

</body>

</html>