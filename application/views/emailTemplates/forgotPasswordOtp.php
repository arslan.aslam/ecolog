<!DOCTYPE html>
<html lang="en">
<head>
  <title>Email</title>
  <meta charset="utf-8">
  <link rel="shortcut icon" type="image/png" href="assets/images/favicon.png"/>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
  <!-- font-family: 'Lato', sans-serif; -->

</head>
<style>
body{
  font-family: Arial, Helvetica, sans-serif !important;
}

</style>
<body>
	<div class="container "  style="padding: 50px 0px; text-align: center;

	">

  <div class="text-center " style="
  background-image: url(<?php echo base_url()?>assets/images/header_email.png);
  background-repeat: no-repeat;
  background-size: cover;
  width:100%;
  max-width: 600px !important;
  margin: 20px auto 0px !important;
  padding: 10px 0px;
  text-align: center;display: inline-block; ">
  <img class="" style="    width: 150px;
  height: 65px;
  object-fit: contain;" src="<?php echo base_url()?>assets/images/logo_email.png">
</div>

<div class="row  text-center" style="
padding: 20px 0px 0px 0px !important;
background: rgb(255,255,255) !important;
box-shadow: inset 0 10px 10px rgba(0, 0, 0, 0.20);
width:100%;
max-width: 600px !important;
margin: -5px auto !important;
text-align: center;
border: 1px solid #eee;">

<div class="col-md-12 ">
  <h4 class="" style="
  color: #468fb1;
  font-size: 25px;
  line-height: 1.167;
  font-weight: 600;
  margin-bottom: 0;
  ">Activate Your</h4>
  <h4 class="" style="
  color: #74a857;
  font-size: 30px;
  line-height: 1.167;
  font-weight: 600;
  margin-top: 0;
  ">Forget Your Password</h4>
</div>



<div class="col-md-12">
  <div>
    <p style="
    font-size: 17px;
    color: #000;
    line-height: 1.2;
    font-weight: 500;">Hello! <?php echo $name; ?>,</p>
    <p style="
    font-size: 14px;
    color: #848484;
    line-height: 1.2;
    letter-spacing: 0.03rem;">
    Looks like you forget your password for favor. <br>
  If this is correct, use below OTP code  to reset your password.<br> Your OTP is: 

  </p>

  <div>
    <p style="
    font-size: 17px;
    color: #000;
    line-height: 1.2;"> <?php echo $otp;?></p>
    <p style="
    font-size: 14px;
    color: #848484;
    line-height: 1.2;margin-top: 0;margin-bottom: 0;">Thanks</p>
    <p style="
    font-size: 16px;
    color: #468fb1;
    line-height: 1.6;
    font-weight: 600;
    letter-spacing: .04rem; margin-top: 2px;margin-bottom:0;">Favor Team</p>

    <p style="
    font-size: 14px;
    color: #848484; margin-top: 0;
    line-height: 1.2;">This is an automated message, please do not reply.</p>
  </div>
  <div style="    background-image: url(<?php echo base_url()?>assets/images/bg_email.png);
  padding: 10px 0px;
  background-repeat: no-repeat;
  background-size: cover;
  box-shadow: 0px -2px 8px 0px rgba(0,0,0,.3);">


  <div >
    <p style="
    font-size: 16px;
    color: #fff;
    line-height: 1;">Stay Connected</p>
  </div>
  <div >
    <ul style="     padding: 0;
    margin: 0;">
    <li style="
    display:inline-block;
    height: 35px;
    width: 35px;
    padding: 3px;
    border-radius: 50px;

    ">

    <a target="_blank" href="#"><img style="
    height: 40px;
    width: 40px;
    padding: 5px 0px;" src="<?php echo base_url()?>assets/images/instagram_email.png"></a></li>
    <li style="
    display:inline-block;
    height: 35px;
    width: 35px;
    padding: 3px;
    border-radius: 50px;
    ">

    <a target="_blank" href="#"><img style="
    height: 40px;
    width: 40px;
    padding: 5px 0px;" src="<?php echo base_url()?>assets/images/facebook_email.png"></a></li>

    <li style="
    display:inline-block;
    height: 35px;
    width: 35px;
    padding: 3px;
    border-radius: 50px;

    ">

    <a target="_blank" href="#"><img style="
    height: 40px;
    width: 40px;
    padding: 5px 0px;" src="<?php echo base_url()?>assets/images/google_email.png"></a></li>

  </ul>
  <div>
   <p style="color: #fff;
   font-size: 14px;
   font-weight: 500;
   letter-spacing: 0.03em;
   padding: 15px 0px;
   margin: 0;">© 2020 favor All Rights Reserved</p>
 </div>
</div>

</div>
</div>




</div>

</div>
</div>











</div>
</body>
</html>
