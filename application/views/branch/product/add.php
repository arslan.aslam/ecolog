<div class="col-12">
	<div class="container-fluid dashboard-content-padding">
		<div class="row row-space dashboard-head-row ">
			<div class="pull-left col-12 col-xs-12 col-sm-12 col-md-12 col-lg-12 col-space text-center ">

				<div class="head">
					<h2 class="text-blackish">Add Products</h2>
				</div>

			</div>
		</div>
	</div>
	<div class="container-fluid dashboard-container bg-white dashboard-content-padding">
		<div class="row content-body-row mb-4">
			<div class="pull-left col-12  pl-auto">
				<div class="label-text bottom-border">
					<h4 class="tablinks tab-active">Product details</h4>
				</div>
			</div>
			<div class="pull-left col-12 col-xs-12 col-sm-12 col-md-6 col-lg-6 order-1 order-xs-1 order-sm-1 order-md-0 order-lg-0  col-space content">
				<div class="custom-row-container">
					<?php echo form_open_multipart('add-product/'.encode(branch)); ?>
					<div class="row row-space">
						<div class="pull-left col-12 col-xs-12 col-sm-12 col-md-12 col-lg-12 col-space">
											<!-- <div class="error-msg"></div>this div for error message
											<div class="block-fieldset input-margin">
												<fieldset class="fieldset-border">
													<legend class="fieldset-border"><strong>Name</strong></legend>
													<input type="text" class="form-input" type="text" placeholder="Enter Name" value="" />
												</fieldset>
											</div> -->
											<div class="error-msg"><?php echo form_error('name'); ?></div><!-- this div for error message -->
											<div class="block-fieldset input-margin">
												<fieldset class="fieldset-border">
													<legend class="fieldset-border"><strong>Product Name</strong></legend>
													<input type="text" name="name" class="form-input" type="text"  placeholder="Enter product name" value="<?php echo $this->input->post('name') ?>" />
												</fieldset>
											</div>
											<div class="error-msg"><?php echo form_error('code'); ?></div><!-- this div for error message -->
											<div class="block-fieldset input-margin">
												<fieldset class="fieldset-border">
													<legend class="fieldset-border"><strong>Product Code</strong></legend>
													<input type="text" name="code" class="form-input" type="text" placeholder="Enter Product Code" value="<?php echo $this->input->post('code') ?>" />
												</fieldset>
											</div>
											<div class="error-msg"><?php echo form_error('quantity'); ?></div><!-- this div for error message -->
											<div class="block-fieldset input-margin">
												<fieldset class="fieldset-border">
													<legend class="fieldset-border"><strong>Quantity</strong></legend>
													<input  class="form-input" name="quantity" type="number"  placeholder="Enter  quantity" value="<?php echo $this->input->post('quantity') ?>"  min="1"/>
												</fieldset>
											</div>

											<div class="error-msg"><?php echo form_error('category'); ?></div><!-- this div for error message -->
											<div class="block-fieldset input-margin">
												<fieldset class="fieldset-border no-padding">
													<legend class="fieldset-border"><strong>Category</strong></legend>
													<i class="select-icon fa fa-angle-down"></i>
													<select name="category" class="select-input">
														<option selected disabled>Select category</option>
														
														<?php foreach ($category as $row) : ?>
															<option value="<?php echo $row['title']; ?>" <?php echo set_select('category',  $row['title']); ?>><?php echo $row['title'] ?></option>
														<?php endforeach; ?>

													</select>
												</fieldset>
											</div>
										</div>

									</div>

								</div>
							</div>


							<!--<div class="pull-left col-2 col-xs-2 col-sm-2 col-md-2 col-lg-2 col-space content "></div>-->
							<!--2nd col-->
							<div class="pull-left col-12 col-xs-12 col-sm-12 col-md-6 col-lg-6  order-0 order-xs-0 order-sm-0 order-md-1 order-lg-1 col-space content text-center">
								<div class="custom-row-container ">							
											<!-- <div class="labeling">
												<h4>Product Image</h4>
											</div> -->
											<div class="profile-pic-div"><div id="imagePreview" class="profile-pic mb-3 productimg">

											</div>
											<div class="edit-profile text-center">
												<label for="edit-profile" class="effect-shine">Select image<input type="file" name="image" id="edit-profile" value="<?php echo $this->input->post('image') ?>"></label></div>
											</div>
											<div class="error-msg"><?php echo form_error('image'); ?></div><!-- this div for error message -->
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-12">
										<div class="btn-filed text-center btn-space">
											<!-- <a class="btn btn-blue-lg btn-hover" href="register.html">CREATE</a> -->
											<button class="btn btn-blue-lg btn-hover" type="submit">SAVE</button>
											<!-- <a href="" class="btn btn-blue-lg btn-hover">SAVE</a> -->
										</div>
									</div>
								</div>
							</div>
						</div>
						<?php echo form_close() ?>
					</div>
				</div>
				<?php if($this->input->post('image')){?>
					<style>
						productimg{
							background: url(<?php echo $this->input->post('image')?>);
						}
					</style>
					<?php } ?>