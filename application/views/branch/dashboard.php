<div class="col-12">
	<div class="container-fluid dashboard-content-padding">
		<div class="row row-space dashboard-head-row ">
			<div class="pull-left col-12 col-xs-12 col-sm-12 col-md-12 col-lg-12 col-space text-center ">

				<div class="head">
					<h2 class="text-blackish">Dashboard</h2>
				</div>

			</div>
		</div>
	</div>

	<div class="row row-space anlytics-row">
		<div class="pull-left col-12 col-xs-12 col-sm-12 col-md-12 col-lg-12 col-space analytic-head">
			<div class="head">
				<h6 class="text-22 text-weight-500 text-blackish">Analytics</h6>
			</div>
		</div>
	</div>
	<div class="container-fluid dashboard-container bg-white dashboard-content-padding">
		<div class="row content-head-row">
			<div class="pull-left col-8 col-xs-8 col-sm-8 col-md-8 col-lg-8  col-space head-col">
				<div class="head">
					<h7 class="text-blackish">Monthly Order growth</h7>
				</div>
			</div>
			<div
			class="pull-left col-4 col-xs-4 col-sm-4 col-md-4 col-lg-4 col-space graph-head table-head-info">
			<ul class="list-style-none text-right">

			</ul>
		</div>
	</div>
	<div class="row content-body-row">
		<div class="pull-left col-12 col-xs-12 col-sm-12 col-md-12 col-lg-12 col-space table-content">
			<div class="table-container graph-show d2">
				<div id='chartDiv1'></div>
			</div>
		</div>
		<div class="pull-left col-12 col-xs-12 col-sm-12 col-md-12 col-lg-12 col-space">
			<div class="table-container graph-show d1">
				<div id='chartDiv2'></div>
			</div>

		</div>
	</div>
</div>

<!-- <div class="container-fluid dashboard-container bg-white dashboard-content-padding mt-4">
	<div class="row content-head-row">
		<div class="pull-left col-8 col-xs-8 col-sm-8 col-md-8 col-lg-8 col-space head-col">
			<div class="head">
				<h7 class="text-blackish">Brand growth</h7>
			</div>
		</div>
		<div
		class="pull-left col-4 col-xs-4 col-sm-4 col-md-4 col-lg-4  col-space graph-head table-head-info">
		<ul class="list-style-none text-right">

		</ul>
	</div>
</div>
<div class="row content-body-row">
	<div class="pull-left col-12 col-xs-12 col-sm-12 col-md-12 col-lg-12 col-space table-content">
		<div class="table-container graph-show d2">
			<div id='chartDiv3'></div>
		</div>
	</div>
	<div class="pull-left col-12 col-xs-12 col-sm-12 col-md-12 col-lg-12 col-space">
		<div class="table-container graph-show d1">
			<div id='chartDiv4'></div>
		</div>

	</div>
</div>
</div> -->


</div>
</div>
</div>
</div>
</div><script src="https://cdn.zingchart.com/zingchart.min.js"></script>
<script>
	$(function () {
		$('.graph-show').hide();
		$('.d2').show();

		$('#select').on("change", function () {
			$('.graph-show').hide();
			$('.d' + $(this).val()).show();
		}).val("2");
	});
</script>
<!-- end change graph -->


<!--line and bar graph js -->
<script>
	/* Unique variable names and chart data */
	var myChart1 = {
		"type": "bar",
		"plot": {
			"backgroundColor": "#4C84FF",
			"border-radius": "6px",
			"tooltip": {
				"backgroundColor": "#3b70e5",
				"height": "50px",
				"width": "100px",
				"border": "1px solid #4C84FF"
			}
		},
		"series": [
		{ "values": [1,2,3,4,5,6,7,8,9,10,11,12] }
		]
	};
	var myChart2 = {
		"type": "line",
		"plot": {
			"backgroundColor": "#4C84FF",
			"tooltip": {
				"backgroundColor": "#3b70e5",
				"height": "50px",
				"width": "100px",
				"border": "1px solid #4C84FF"
			}
		},
		"series": [
		{ "values": [14, 29, 18, 20, 34, 14, 29, 18, 20, 34] }
		]
	};
	/* Your render methods are added after this. */

	/* Your variables and chart data are added before this. */
	window.onload = function () {

			// Render methods, which reference your charts div element identifiers and variable names.

			zingchart.render({
				id: 'chartDiv1',
				data: myChart1,
				height: 500
			});
			zingchart.render({
				id: 'chartDiv2',
				data: myChart2,
				height: 500
			});
			zingchart.render({
				id: 'chartDiv3',
				data: myChart1,
				height: 500
			});
			zingchart.render({
				id: 'chartDiv4',
				data: myChart2,
				height: 500
			});

		};


		var myDashboard = {
			"graphset": [
			{
				"type": "line",
				/* Size your chart using height/width attributes */
				"height": "30%",
				"width": "90%",
				/* Position your chart using x/y attributes */
				"x": "5%",
				"y": "5%",
				"series": [
				{ "values": [11, 21, 19, 54, 6, 19, 19, 59] }
				]
			},
			{
				"type": "funnel",
				/* Size your chart using height/width attributes */
				"height": "55%",
				"width": "40%",
				/* Position your chart using x/y attributes */
				"x": "5%",
				"y": "40%",
				"series": [
				{ "values": [30] },
				{ "values": [15] },
				{ "values": [5] },
				{ "values": [3] }
				]
			},
			{
				"type": "radar",
				"height": "55%",
				"width": "45%",
				"x": "50%",
				"y": "40%",
				"series": [
				{ "values": [20, 87, 59, 85, 54] }
				]
			}
			]
		};
	</script>