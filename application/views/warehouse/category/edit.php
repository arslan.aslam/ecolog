<div class="col-12">
    <div class="container-fluid dashboard-content-padding">
     <div class="row row-space dashboard-head-row ">
      <div class="pull-left col-12 col-xs-12 col-sm-12 col-md-12 col-lg-12 col-space text-center ">

       <div class="head">
        <h2 class="text-blackish">Edit Category</h2>
    </div>

</div>
</div>
</div>
<div class="container-fluid dashboard-container bg-white dashboard-content-padding">
 <div class="row content-body-row mb-4">
  <div class="pull-left col-12 pl-auto">
   <div class="label-text bottom-border ">
    <h4 class="tablinks tab-active">update details</h4>
</div>
</div>
<div class="pull-left col-12 col-xs-12 col-sm-12 col-md-6 col-lg-6 order-1 order-xs-1 order-sm-1 order-md-0 order-lg-0  col-space content">
   <div class="custom-row-container">
    <?php echo form_open_multipart('edit-category/'.encode($category['id']),'name="form"'); ?>
     <div class="row row-space">
      <div class="pull-left col-12 col-xs-12 col-sm-12 col-md-12 col-lg-12 col-space">
       <div class="error-msg"> <?php echo form_error('title'); ?></div><!-- this div for error message -->
       <div class="block-fieldset input-margin">
        <fieldset class="fieldset-border">
         <legend class="fieldset-border"><strong>Title</strong></legend>
         <input type="text" class="form-input" name="title" type="text"  placeholder="Enter title" value="<?php echo ($this->input->post('title') ? $this->input->post('title') : $category['title']); ?>"  />
     </fieldset>
 </div>

 <div class="error-msg"><?php echo form_error('description'); ?></div><!-- this div for error message -->											
 <div class="block-fieldset input-margin">
    <fieldset class="fieldset-border">
     <legend class="fieldset-border"><strong>Description</strong></legend>
     <textarea class="textarea-input" placeholder="Enter Description....." name="description" rows="5" ><?php echo ($this->input->post('description') ? $this->input->post('description') : $category['description']); ?></textarea>
 </fieldset>
</div>
</div>

</div>
</div>
</div>
<!--<div class="pull-left col-2 col-xs-2 col-sm-2 col-md-2 col-lg-2 col-space content "></div>-->
<!--2nd col-->
<div class="pull-left col-12 col-xs-12 col-sm-12 col-md-6 col-lg-6  order-0 order-xs-0 order-sm-0 order-md-1 order-lg-1 col-space content text-center">
   <div class="custom-row-container ">								
      <div class="profile-pic-div"><div id="imagePreview" style="background-image:url(<?php echo base_url($this->config->item('imageUploadPath') . $category['image'])?>)" class="profile-pic mb-3">

      </div>
      <div class="edit-profile text-center">
       <label for="edit-profile" class="effect-shine">Select image<input type="file" accept="image/*" name="image" id="edit-profile" value="<?php echo base_url($this->config->item('imageUploadPath').($this->input->post('image')?$this->input->post('image'):$category['image']))?>"></label></div>
       <div class="error-msg"><?php echo form_error('image'); ?></div><!-- this div for error message --> 
   </div>

</div>
</div>
</div>
<div class="row">
  <div class="col-12">
   <div class="btn-filed text-center btn-space">
    <!-- <a class="btn btn-blue-lg btn-hover" href="register.html">CREATE</a> -->
    <button class="btn btn-blue-lg btn-hover" type="submit">SAVE</button>
    <!-- <a href="category.html" >SAVE</a> -->
   <?php echo form_close() ?>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
<!-- <?php if($category['image']){ ?>
<style>
	.myimage
	{
		background-image: url(<?php echo base_url().'uploads/'.$category['image'] ?>);
	}
</style>
<?php } ?> -->