<div class="col-12">
    <div class="container-fluid dashboard-content-padding">
       <div class="row row-space dashboard-head-row ">
          <div class="pull-left col-12 col-xs-12 col-sm-12 col-md-12 col-lg-12  text-center ">
             
             <div class="head">
                <h2 class="text-blackish">Products</h2>
            </div>
            
        </div>
    </div>
</div>


<div class="container-fluid padding-0  analytic-container">	
    <div class="container-fluid dashboard-container bg-white dashboard-content-padding">
       <div class="row content-head-row mb-5">
          <div class="pull-left col-12 col-xs-12 col-sm-4 col-md-4 col-lg-4 head-col">
             <div class="head">
                <ul class="list-style-none content-tabs">
                 
                 
                </ul>
            </div>
            
            
        </div>
        <div class="pull-left col-12 col-xs-12 col-sm-8 col-md-8 col-lg-8  table-head-info text-right">
         <div class="container-fluid">
            <ul class="list-style-none content-tabs ">
               <a href="<?php echo base_url('add-product/'.encode(warehouse)) ?>" class="btn btn-invite" >Add new
               + </a>
               
           </ul>
       </div>
   </div>
</div>

<div class="row content-body-row">
  <div class="pull-left col-12 col-xs-12 col-sm-12 col-md-12 col-lg-12  table-content">
     <div class="table-container tabcontent show-tab" id="add-driver">
        <div class="table-responsive">
           <table class="table table-hover" id="table1">
              <thead class="table-head">
                 <tr>
                    <th>
                       Name
                   </th>
                   <th>Product</th>
                   <th>Quantity</th>
                   <th>Category</th>
                   <th class="text-center">Remove</th>
               </tr>
           </thead>
           <tbody class="table-body">
             <tr>
                <td class="w-30">
                   <span class="svg-block pr-2">
                      <img class="img__icon" src="../../assets/images/img-profile.png" alt="" srcset="">
                  </span>
              John Doe</td>
              <td><span class="text-light-pink text-12">#234 &nbsp;  </span>  A5-Jordan</td>
              <td>4</td>
              <td>Shoes</td>


              <td>
               <div class="table-icon text-center">
                  <!-- <img src="../../assets/images/img-delete.png"> -->
                  <div class="svg-block">
                     <svg class="svg-icon" xmlns="http://www.w3.org/2000/svg"
                     viewBox="0 0 16.945 23.107">
                     <g id="Delete__x2F__Trash"
                     transform="translate(-35.796)">
                     <path id="Path_57" data-name="Path 57"
                     d="M38.149,21.539a2.1,2.1,0,0,0,2.116,1.568h8.011a2.1,2.1,0,0,0,2.116-1.568L51.972,6.162h-15.4Zm9.2-13.066a.77.77,0,0,1,1.541,0l-.77,11.554a.77.77,0,1,1-1.54,0Zm-3.851,0a.77.77,0,1,1,1.54,0V20.027a.77.77,0,1,1-1.54,0ZM40.418,7.7a.77.77,0,0,1,.77.77l.77,11.554a.77.77,0,0,1-1.541,0l-.77-11.554A.77.77,0,0,1,40.418,7.7ZM51.51,3.082H48.121V1.541C48.121.372,47.743,0,46.581,0H41.959a1.347,1.347,0,0,0-1.54,1.541V3.082H37.029A1.2,1.2,0,0,0,35.8,4.238a1.2,1.2,0,0,0,1.232,1.156H51.51a1.2,1.2,0,0,0,1.232-1.156A1.2,1.2,0,0,0,51.51,3.082Zm-4.93,0H41.959V1.54h4.621V3.082Z"
                     fill="#e86572" fill-rule="evenodd" />
                 </g>
             </svg>

         </div>
     </div>
 </td>
</tr>

<tr>
    <td class="w-30">
       <span class="svg-block pr-2">
          <img class="img__icon" src="../../assets/images/img-profile.png" alt="" srcset="">
      </span>
  John Doe</td>
  <td><span class="text-light-pink text-12">#576 &nbsp;  </span> Iron Skillet</td>
  <td>4</td>
  <td>Culinary</td>

  <td>
   <div class="table-icon text-center">
      <!-- <img src="../../assets/images/img-delete.png"> -->
      <div class="svg-block">
         <svg class="svg-icon" xmlns="http://www.w3.org/2000/svg"
         viewBox="0 0 16.945 23.107">
         <g id="Delete__x2F__Trash"
         transform="translate(-35.796)">
         <path id="Path_57" data-name="Path 57"
         d="M38.149,21.539a2.1,2.1,0,0,0,2.116,1.568h8.011a2.1,2.1,0,0,0,2.116-1.568L51.972,6.162h-15.4Zm9.2-13.066a.77.77,0,0,1,1.541,0l-.77,11.554a.77.77,0,1,1-1.54,0Zm-3.851,0a.77.77,0,1,1,1.54,0V20.027a.77.77,0,1,1-1.54,0ZM40.418,7.7a.77.77,0,0,1,.77.77l.77,11.554a.77.77,0,0,1-1.541,0l-.77-11.554A.77.77,0,0,1,40.418,7.7ZM51.51,3.082H48.121V1.541C48.121.372,47.743,0,46.581,0H41.959a1.347,1.347,0,0,0-1.54,1.541V3.082H37.029A1.2,1.2,0,0,0,35.8,4.238a1.2,1.2,0,0,0,1.232,1.156H51.51a1.2,1.2,0,0,0,1.232-1.156A1.2,1.2,0,0,0,51.51,3.082Zm-4.93,0H41.959V1.54h4.621V3.082Z"
         fill="#e86572" fill-rule="evenodd" />
     </g>
 </svg>

</div>
</div>
</td>
</tr>


</tbody>
</table>
</div>
</div>




</div>

</div>
</div>
</div>
</div>
</div>
</div>