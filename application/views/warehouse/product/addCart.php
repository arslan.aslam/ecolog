<!--Side bar-->
	
    
        <div class="sidebar--modifier cart--modifier--menu">
            <div class="container-fluid">
               
                <div class="row">
                    <div class="col-12 px-0">
						<div class="box-shadow--cart">
                            <a class="close-cart" id="close-cart"><img src="../../assets/images/cancel.png" alt=""></a>

							<h4>
								<span class="">Categories</span>  
							</h4>
                        <div class="sidebar-accordian sidebar-cart--modifier ">
							
                            <div class="side-menus-list pt-0">
								
                                <!-- Contenedor -->
                                <ul id="accordion" class=" accordion">
									
                                    <li class="sidebar-menu sidebar-menu--modifier  ">
                                        <a class="link text-16 text-light-pink" href="manager.html">
                                            <span class="svg-block">
                                            </span>
                                            Footwear
                                        </a>
                                    </li>
                                    <li class="sidebar-menu sidebar-menu--modifier cart-dashboard-active">
                                        <a class="link text-16 text-light-pink" >
                                            <span class="svg-block">
                                            </span>
                                            Culnry
                                        </a>
                                    </li>
                                    
                                    <li class="sidebar-menu  sidebar-menu--modifier">
										<a class="link text-16 text-light-pink" href="#">
                                            <span class="svg-block">
                                            </span>
                                            Makeup
                                        </a>
                                    </li>
                                    <li class="sidebar-menu sidebar-menu--modifier">
                                        <a class="link text-16 text-light-pink" href="driver.html">
                                            <span class="svg-block">
                                            </span>
                                            Googles
                                        </a>
                                    </li>
                                    
    
                                    <li class="sidebar-menu sidebar-menu--modifier">
                                        <a class="link text-16 text-light-pink" href="branches.html">
                                            <span class="svg-block">
                                            </span>
                                            Kitchen 
                                        </a>
                                    </li>
                                    <li class="sidebar-menu sidebar-menu--modifier" >
                                        <a class="link text-16 text-light-pink" href="settings.html">
                                            <span class="svg-block">
                                            </span>
                                            Furniture
                                        </a>
                                    </li>
                                    <li class="sidebar-menu sidebar-menu--modifier">
                                        <a class="link text-16 text-light-pink" data-toggle="modal" data-target="#reject-request">
                                            <span class="svg-block">
                                            </span>
                                            Watches
                                        </a>
                                </ul>
                            </div>
						</div>
						</div>
                    </div>
                </div>
            </div>
       
    	</div>
    <div class="container-fluid  main-container">
		<div class="row row-streched">
			<!-- end side bar -->
			<div class="col-12">
                <div class="container-fluid dashboard-content-padding">
					<div class="row row-space dashboard-head-row ">
						<div class="pull-left col-12 col-xs-12 col-sm-12 col-md-12 col-lg-12 col-space text-center ">
			
							<div class="head">
								<h2 class="text-blackish">Cart</h2>
							</div>
                           
						</div>
					</div>
                </div>
                
                <div class="row content-head-row bottom-border ">
				<div class="container-fluid dashboard-container dashboard-content-padding ">
					
						<div class=" col-12">
							<div class="label-text bottom-border cart-label">
                                <h4>Product Details</h4>
                                <span class="Hamburger cart-hamburger">
                                   <a>+ View Cart</a>
                                </span>
                            </div>
                          
						</div>
						<div class="col-12 mt-4 ">
							<ul class="cart-items-select">
								<li class="cart-item-select-list">
									<div class="cart-item-select-list-product"><span class="text-black">#234 &nbsp;</span><span class="text-black">A5-Jordan</span>  </div> 
									<div class="cart-item-select--removed"><a href="#" class="btn-xs"><img  src="../../assets/images/cancel.png" alt=""></a> </div>
								</li>
								<li class="cart-item-select-list">
									<div class="cart-item-select-list-product"><span class="text-black">#233 &nbsp;</span><span class="text-black">A6-Jordan</span>  </div> 
									<div class="cart-item-select--removed"><a href="#" class="btn-xs"><img  src="../../assets/images/cancel.png" alt=""></a> </div>
								</li>
								<li class="cart-item-select-list">
									<div class="cart-item-select-list-product"><span class="text-black">#236 &nbsp;</span><span class="text-black">A8-Jordan</span>  </div> 
									<div class="cart-item-select--removed"><a href="#" class="btn-xs"><img  src="../../assets/images/cancel.png" alt=""></a> </div>
								</li>
								<li class="cart-item-select-list">
									<div class="cart-item-select-list-product"><span class="text-black">#237 &nbsp;</span><span class="text-black">A10-windy</span>  </div> 
									<div class="cart-item-select--removed"><a href="#" class="btn-xs"><img  src="../../assets/images/cancel.png" alt=""></a> </div>
								</li>
								<li class="cart-item-select-list">
									<div class="cart-item-select-list-product"><span class="text-black">#238 &nbsp;</span><span class="text-black">A2-Jazzy</span>  </div> 
									<div class="cart-item-select--removed"><a href="#" class="btn-xs"><img  src="../../assets/images/cancel.png" alt=""></a> </div>
								</li>
								<li class="cart-item-select-list">
									<div class="cart-item-select-list-product"><span class="text-black">#239 &nbsp;</span><span class="text-black ">A35-Jordan2</span>  </div> 
									<div class="cart-item-select--removed"><a href="#" class="btn-xs"><img src="../../assets/images/cancel.png" alt=""></a> </div>
								</li>
								<li class="cart-item-select-list">
									<div class="cart-item-select-list-product"><span class="text-black">#234 &nbsp;</span><span class="text-black">A5-Jordan</span>  </div> 
									<div class="cart-item-select--removed"><a href="#" class="btn-xs"><img  src="../../assets/images/cancel.png" alt=""></a> </div>
								</li>
							</ul>
                        </div>
                        <div class="col-12 mt-4">
                            <div class="label-text">
                                <h4>
                                    <span class="categor-name--head">Category</span> : <span class="text-black">Culnary</span>
                                </h4>
                            </div>
                        </div>  
						<div class="col-12">
                            <div class="row">
                                <div class="col-12 col-sm-6 col-md-6 col-lg-3">
                                    <div class="card" >
                                        <div class="box">
                                                <img class="card-img-top" src="../../assets/images/dummy/eco-1024 crop.png" alt="Card image cap">
                                            <div class="text">
                                                <h4>Casual Shirts <span>( 6 )</span></h4>
                                            </div>
                                        </div>
                                       
                                        <div class="card-body">
                                        
                                            <div class="row product-details-card my-auto">
                                                <div class="col-5 col-lg-12 col-xl-5 p-0">
                                                    <div class="text-footer text-left">
                                                        <p class="m-0">XYZ-123</p>
                                                      </div>
                                                    </div>
                                                <div class=" col-7 col-lg-12 col-xl-7 increment-decrement-input text-right p-0">
                                                        <form class="m-0">
                                                            <span class="value-button value-button--modifier quantity__minus" id="decrease"  value="Decrease Value"><img src="../../assets/images/minus.png" alt=""></span>
                                                            <input type="number" id="number1" class="number-modifier quantity__input" value="0" />
                                                            <span class="value-button value-button--modifier quantity__plus" id="increase" value="Increase Value"><img src="../../assets/images/plus.png" alt=""></span>
                                                        </form>
                                                    </div>
                                                </div>
                                                  

                                                
                                               
                                              
                                          
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-sm-6 col-md-6 col-lg-3">
                                    <div class="card" >
                                        <div class="box">
                                                <img class="card-img-top" src="../../assets/images/dummy/shite-barc.png" alt="Card image cap">
                                            <div class="text">
                                                <h4>Casual Shirts <span>( 6 )</span></h4>
                                            </div>
                                        </div>
                                       
                                        <div class="card-body">
                                        
                                            <div class="row product-details-card my-auto">
                                                <div class="col-5 col-lg-12 col-xl-5 p-0">
                                                    <div class="text-footer text-left">
                                                        <p class="m-0">XYZ-123</p>
                                                      </div>
                                                    </div>
                                                <div class=" col-7 col-lg-12 col-xl-7 increment-decrement-input text-right p-0">
                                                        <form class="m-0">
                                                            <span class="value-button value-button--modifier  quantity__minus" id="decrease"  value="Decrease Value"><img src="../../assets/images/minus.png" alt=""></span>
                                                            <input type="number" id="number1" class="number-modifier quantity__input" value="0" />
                                                            <span class="value-button value-button--modifier quantity__plus" id="increase"  value="Increase Value"><img src="../../assets/images/plus.png" alt=""></span>
                                                        </form>
                                                    </div>
                                                </div>
                                                  

                                                
                                               
                                              
                                          
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-sm-6 col-md-6 col-lg-3">
                                    <div class="card" >
                                        <div class="box">
                                                <img class="card-img-top" src="../../assets/images/dummy/white-shirt.png" alt="Card image cap">
                                            <div class="text">
                                                <h4>Casual Shirts <span>( 6 )</span></h4>
                                            </div>
                                        </div>
                                       
                                        <div class="card-body">
                                        
                                            <div class="row product-details-card my-auto">
                                                <div class="col-5 col-lg-12 col-xl-5 p-0">
                                                    <div class="text-footer text-left">
                                                        <p class="m-0">XYZ-123</p>
                                                      </div>
                                                    </div>
                                                <div class=" col-7 col-lg-12 col-xl-7 increment-decrement-input text-right p-0">
                                                        <form class="m-0">
                                                            <span class="value-button value-button--modifier quantity__minus" id="decrease" ><img src="../../assets/images/minus.png" alt=""></span>
                                                            <input type="number" id="number1" class="number-modifier quantity__input" value="0" />
                                                            <span class="value-button value-button--modifier quantity__plus" id="increase" ><img src="../../assets/images/plus.png" alt=""></span>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                </div>
                                <div class="col-12 col-sm-6 col-md-6 col-lg-3">
                                    <div class="card" >
                                        <div class="box">
                                                <img class="card-img-top" src="../../assets/images/shoes.jpg" alt="Card image cap">
                                            <div class="text">
                                                <h4>Casual Shirts <span>( 6 )</span></h4>
                                            </div>
                                        </div>
                                       
                                        <div class="card-body">
                                        
                                            <div class="row product-details-card my-auto">
                                                <div class="col-5 p-0">
                                                    <div class="text-footer text-left">
                                                        <p class="m-0">XYZ-123</p>
                                                      </div>
                                                    </div>
                                                <div class=" col-7 increment-decrement-input text-right p-0">
                                                        <form class="m-0">
                                                            <span class="value-button value-button--modifier quantity__minus" id="decrease"  value="Decrease Value"><img src="../../assets/images/minus.png" alt=""></span>
                                                            <input type="number" id="number1" class="number-modifier quantity__input" value="0" />
                                                            <span class="value-button value-button--modifier quantity__plus" id="increase"  value="Increase Value"><img src="../../assets/images/plus.png" alt=""></span>
                                                        </form>
                                                    </div>
                                                </div> 
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
						<div class="col-12">
							<ul class="list-style-none text-center text-sm-right">
								<li class="list-inline-item list-inline-item--cart-modifier">
									<div class="btn-filed  btn-space">
										<a href="#" class="btn btn-red-sm  bt-cart--modifier default-properties">Update Cart</a>
									</div>
								</li>
							</ul>
						</div>
						
						
						<!--<div class="col-12 pt-4">
							<ul class="list-style-none text-center">
								<li class="list-inline-item">
									<div class="btn-filed text-center btn-space btn-space-accept">
										<a href="" class="btn btn-green-lg btn-hover bt-cart--modifier">Accept</a>
									</div>
								</li>
								<li class="list-inline-item">
									<div class="text-center btn-space btn-space-reject">
										<a href="#" class="btn btn-blue-lg btn-hover bt-cart--modifier">Reject</a>
									</div>
								</li>
							</ul>
						</div> -->
					</div>
				</div>
            </div>
		</div>
	</div>