
 <div class="col-12">
    <div class="container-fluid dashboard-content-padding">
        <div class="row row-space dashboard-head-row ">
            <div class="pull-left col-12 col-xs-12 col-sm-12 col-md-12 col-lg-12 col-space text-center ">

                <div class="head">
                    <h2 class="text-blackish">Chnage Password</h2>
                </div>

            </div>
        </div>
    </div>
    <div class="container-fluid dashboard-container-new bg-white dashboard-content-padding">

        <div class="row content-body-row">
            <div class="col-12 col-xs-12 col-sm-12 col-md-12 col-lg-12 col-space table-content">

                <?php echo form_open('change-password/'.encode(warehouse))?>

                <div class="row row-space">
                    <div class="pull-left col-12 col-sm-12 col-md-8 table-content m-0 m-md-auto">
                        <div class="error-msg"><?php echo form_error('oldPassword'); ?> </div> <!-- this div for error message -->
                        <div class="block-fieldset input-margin">
                            <fieldset class="fieldset-border">
                                <legend class="fieldset-border">Old Password</legend>
                                <input type="password" class="form-input"  placeholder="Enter old password" name="oldPassword" value="<?php echo $this->input->post('oldPassword') ?>" />
                            </fieldset>
                        </div>
                    </div>
                </div>
                <div class="row row-space ">
                    <div class="pull-left col-12 col-sm-12 col-md-8 table-content m-0 m-md-auto">
                        <div class="error-msg"><?php echo form_error('newPassword'); ?> </div> <!-- this div for error message -->
                        <div class="block-fieldset input-margin">
                            <fieldset class="fieldset-border">
                                <legend class="fieldset-border">New Password</legend>
                                <input type="password" class="form-input"  placeholder="Enter new password" name="newPassword" value="<?php echo $this->input->post('newPassword') ?>" />
                            </fieldset>

                        </div>
                    </div>
                </div>

                <div class="row row-space">
                    <div class="pull-left col-12 col-sm-12 col-md-8 table-content m-0 m-md-auto">
                        <div class="error-msg"><?php echo form_error('confPassword'); ?> </div> <!-- this div for error message -->
                        <div class="block-fieldset input-margin">
                            <fieldset class="fieldset-border">
                                <legend class="fieldset-border">Re-Type Password</legend>
                                <input type="password" class="form-input"  placeholder="Enter new password" name="confPassword" value="<?php echo $this->input->post('confPassword') ?>" />
                            </fieldset>
                        </div>
                    </div>
                    </div>

                <div class="row row-space text-center">
                    <div class="pull-left col-12 col-xs-12 col-sm-12 col-md-12 col-lg-12 col-space">
                        <button type="submit" class="btn-filed text-center btn-space btn btn-blue-lg btn-hover" >Save
                        </button>

                    </div>
                </div>

                <?php echo form_close() ?>



            </div>

        </div>
    </div>
</div>