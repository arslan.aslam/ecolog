<div class="col-12">
  <div class="container-fluid dashboard-content-padding">
    <div class="row row-space dashboard-head-row ">
      <div class="pull-left col-12 col-xs-12 col-sm-12 col-md-12 col-lg-12 col-space text-center ">

        <div class="head">
          <h2 class="text-blackish">Add Branch</h2>
        </div>

      </div>
    </div>
  </div>
  <div class="container-fluid dashboard-container-new bg-white dashboard-content-padding">

    <div class="row content-body-row">
      <div class="pull-left col-12 col-xs-12 col-sm-12 col-md-12 col-lg-12 col-space table-content">
        <?php $formPath = 'add-manager/' . encode(branch);?>
        <?php echo form_open_multipart($formPath) ?>
        <div class="row row-space">
          <div class="pull-left col-12 col-xs-12 col-sm-6 col-md-6 col-lg-6 col-space">
            <div class="error-msg"></div> <!-- this div for error message -->
            <div class="block-fieldset input-margin">
              <fieldset class="fieldset-border">
                <legend class="fieldset-border">First Name</legend>
                <input type="text" class="form-input" name="firstName" placeholder="" value="<?php echo $this->input->post('firstName'); ?>" />
              </fieldset>
              <div class="error-msg"><?php echo form_error('firstName'); ?></div> <!-- this div for error message -->
            </div>
          </div>
          <div class="pull-left col-12 col-xs-12 col-sm-6 col-md-6 col-lg-6 col-space">
            <div class="error-msg"></div> <!-- this div for error message -->
            <div class="block-fieldset input-margin">
              <fieldset class="fieldset-border">
                <legend class="fieldset-border">Last Name</legend>
                <input type="text" class="form-input" name="lastName" placeholder="" value="<?php echo $this->input->post('lastName'); ?>" />
              </fieldset>
              <div class="error-msg"><?php echo form_error('lastName'); ?></div> <!-- this div for error message -->
            </div>
          </div>
        </div>


        <div class="row row-space">
          <div class="pull-left col-12 col-xs-12 col-sm-6 col-md-6 col-lg-6 col-space">
            <div class="error-msg"></div> <!-- this div for error message -->
            <div class="block-fieldset input-margin">
              <fieldset class="fieldset-border">
                <legend class="fieldset-border">Email</legend>
                <input type="text" class="form-input" name="email" placeholder="" value="<?php echo $this->input->post('email'); ?>" />
              </fieldset>
              <div class="error-msg"><?php echo form_error('email'); ?></div> <!-- this div for error message -->

            </div>
          </div>

          <div class="pull-left col-12 col-xs-12 col-sm-6 col-md-6 col-lg-6 col-space">
            <div class="error-msg"></div> <!-- this div for error message -->
            <div class="block-fieldset input-margin">
              <fieldset class="fieldset-border">
                <legend class="fieldset-border">Phone</legend>
                <input type="text" class="form-input" name="phone" placeholder="" oninput="this.value=this.value.replace(/[^0-9]/g,'');" value="<?php echo $this->input->post('phone'); ?>" />
              </fieldset>
              <div class="error-msg"><?php echo form_error('phone'); ?></div> <!-- this div for error message -->
            </div>
          </div>
        </div>


        <div class="row row-space">
          <div class="pull-left col-12 col-xs-12 col-sm-6 col-md-6 col-lg-6 col-space">
            <div class="error-msg"></div> <!-- this div for error message -->
            <div class="block-fieldset input-margin">
              <fieldset class="fieldset-border">
                <legend class="fieldset-border">Password</legend>
                <input type="text" class="form-input" name="password" placeholder="" value="<?php echo $this->input->post('password'); ?>" />
              </fieldset>
              <div class="error-msg"><?php echo form_error('password'); ?></div> <!-- this div for error message -->

            </div>
          </div>

          <div class="pull-left col-12 col-xs-12 col-sm-6 col-md-6 col-lg-6 col-space">
            <div class="error-msg"></div> <!-- this div for error message -->
            <div class="block-fieldset input-margin">
              <fieldset class="fieldset-border">
                <legend class="fieldset-border">Confirm Password</legend>
                <input type="text" class="form-input" name="confPassword" placeholder="" value="<?php echo $this->input->post('confPassword'); ?>" />
              </fieldset>
              <div class="error-msg"><?php echo form_error('confPassword'); ?></div> <!-- this div for error message -->
            </div>
          </div>
        </div>

        <div class="row row-space">
          <div class="pull-left col-12 col-xs-12 col-sm-6 col-md-6 col-lg-6 col-space">
        <div class="error-msg"></div> <!-- this div for error message -->
        <div class="block-fieldset input-margin">
          <fieldset class="fieldset-border">
            <legend class="fieldset-border">Address</legend>
            <input type="text" id="address" class="form-input" name="address" placeholder="Enter and Select valid Google address" value="<?php echo $this->input->post('address'); ?>" />
          </fieldset>
          <div class="error-msg"><?php echo form_error('address'); ?></div> <!-- this div for error message -->
        </div>
      </div>
      <input style="height: 10%" type="text" name="latitude" id="latitude" value="<?php echo $this->input->post('latitude'); ?>">
      <input style="height: 10%" type="text" name="longitude" id="longitude" value="<?php echo $this->input->post('longitude'); ?>">
        </div>
          <input type="hidden" name="userType" value="<?php echo $userType; ?>" />
          <div class="row row-space text-center">
            <div class="pull-left col-12 col-xs-12 col-sm-12 col-md-12 col-lg-12 col-space">
              <button type="submit" class="btn btn-blue-lg btn-hover">Save
              </button>

            </div>
          </div>

          <?php echo form_close() ?>



        </div>

      </div>
    </div>
  </div>