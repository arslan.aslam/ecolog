<div class="limiter">
	<div class="container-login100">
		<div class="wrap-login100  py-3 px-3 py-sm-5 px-sm-5 shadow-row">
			<div class="login-header">
				<div class="container-fluid">
					<div class="row">
						<div class="col-12">
							<div class="header-logo">
								<img src="<?php echo base_url('assets/images/logo1.png') ?>" alt="">
							</div>
						</div>
					</div>
				</div>
			</div>
			<form class="login100-form validate-form " method="post" action="<?php echo base_url('login') ?>">
				<div class="login100-form-title pb-5 mt-3">
					<h2>Login</h2>
				</div>
				<input type="hidden" name="submitted" value="login">
				<div class="wrap-input100 input-div validate-input m-b-10 mt-5" data-validate="Enter username">
					<label for="pass">Email</label>
					<input class="input100" type="email" name="email" placeholder="Enter email" value="<?php echo $this->input->post('email') ?>">
					<span class="focus-input100"></span>
				</div>
				<span class="text-danger"><?php echo form_error('email') ?></span>

				<div class="wrap-input100 input-div validate-input m-b-10 mt-5" data-validate="Enter password">
					<label for="pass">Password</label>
					<input class="input100" type="password" name="password" placeholder=".......">
					<span class="focus-input100"></span>
				</div>
				<span class="text-danger"><?php echo form_error('password') ?></span>

				<div class="container-login100-form-btn mt-5">
					<button type="submit" class="login100-form-btn w-75">
						Login
					</button>
				</div>
			</form>


			<ul class="login100-form login-more pt-3 mt-2">
				<li class="m-b-8">
					<span class="txt1">
						Forgot
					</span>

					<a href="<?php echo base_url('forgot-password') ?>" class="txt2">
						Password?
					</a>
				</li>

			</ul>
		</div>
	</div>
</div>


	<div id="dropDownSelect1"></div>