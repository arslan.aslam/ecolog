	
<div class="limiter">
	<div class="container-login100">
		<div class="wrap-login100  py-3 px-3 py-sm-5 px-sm-5 shadow-row">
			<div class="login-header">
				<div class="container-fluid">
					<div class="row">
						<div class="col-12">
							<div class="header-logo">
								<img src="<?php echo base_url('assets/images/logo1.png') ?>" alt="">
							</div>
						</div>
					</div>
				</div>
			</div>
			<form class="login100-form validate-form " method="post" action="<?php echo base_url('User/changePassword') ?>">
				<div class="login100-form-title pb-5 mt-3">
					<h2>Update Password</h2>
				</div>

				<input type="hidden" name="token" value="<?php echo $token ?>">

				<div class="wrap-input100 input-div validate-input mt-5 m-b-10" data-validate = "Enter Password">
					<label for="pass">Password</label>
					<input class="input100" type="password" name="password" placeholder="Enter Password">
					<span class="focus-input100" ></span>
				</div>
				<div class="error-msg"  style="color:red";><?php echo form_error('password'); ?></div>
				
				<div class="wrap-input100 input-div validate-input mt-5 m-b-10" data-validate = "Enter Password">
					<label for="pass">Confirm Password</label>
					<input class="input100" type="password" name="confPassword" placeholder="Confirm Password">
					<span class="focus-input100" ></span>
				</div>
				<div class="error-msg" style="color:red"><?php echo form_error('confPassword'); ?></div>

				<div class="container-login100-form-btn mt-5">
					<button type="submit"  class="login100-form-btn w-75">
						Reset Password
					</button>
				</div>
			</form>
		</div>
	</div>
</div>


<div id="dropDownSelect1"></div>