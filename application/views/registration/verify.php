	<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100  py-3 px-3 py-sm-5 px-sm-5 shadow-row">
				<div class="login-header">
					<div class="container-fluid">
						<div class="row">
							<div class="col-12 text-center">
								<div class="header-logo">
									<img src="<?php echo base_url('assets/images/app-icon.png') ?>" style="max-width:150px;" alt="">
								</div>
							</div>
						</div>
					</div>
                </div>
				<div class="login100-form validate-form ">
					<div class="login100-form-title pb-5 mt-3">
                        <h2>Account verified</h2>
                        <p><?php echo $message;?></p>
					</div>
				</div>
			</div>
		</div>
	</div>
	

	<div id="dropDownSelect1"></div>