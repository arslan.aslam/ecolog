<script>
	function readURL(input) {
		if (input.files && input.files[0]) {
			var reader = new FileReader();
			reader.onload = function(e) {
				$('#imagePreview').css('background-image', 'url('+e.target.result +')');
				$('#imagePreview').hide();
				$('#imagePreview').fadeIn(650);
			}
			reader.readAsDataURL(input.files[0]);
		}
	}
	$("#imageUpload").change(function() {
		readURL(this);
	});


</script>
<!-- <script type=text/javascript>
	window.onload = function() {

		var file = document.getElementById("thefile");
		var audio = document.getElementById("audio");

		file.onchange = function() {
			var files = this.files;
			audio.src = URL.createObjectURL(files[0]);
			audio.load();
			audio.play();
			var context = new AudioContext();
			var src = context.createMediaElementSource(audio);
			var analyser = context.createAnalyser();

			var canvas = document.getElementById("canvas");
			canvas.width = window.innerWidth;
			canvas.height = window.innerHeight;
			var ctx = canvas.getContext("2d");

			src.connect(analyser);
			analyser.connect(context.destination);

			analyser.fftSize = 25;

			var bufferLength = analyser.frequencyBinCount;
			console.log(bufferLength);

			var dataArray = new Uint8Array(bufferLength);

			var WIDTH = canvas.width;
			var HEIGHT = canvas.height;

			var barWidth = (WIDTH / bufferLength) * 2.5;
			var barHeight;
			var x = 0;

			function renderFrame() {
				requestAnimationFrame(renderFrame);

				x = 0;

				analyser.getByteFrequencyData(dataArray);

				ctx.fillStyle = "#000";
				ctx.fillRect(0, 0, WIDTH, HEIGHT);

				for (var i = 0; i < bufferLength; i++) {
					barHeight = dataArray[i];

					var r = barHeight + (25 * (i/bufferLength));
					var g = 250 * (i/bufferLength);
					var b = 50;

					ctx.fillStyle = "rgb(" + r + "," + g + "," + b + ")";
					ctx.fillRect(x, HEIGHT - barHeight, barWidth, barHeight);

					x += barWidth + 1;
				}
			}

			audio.play();
			renderFrame();
		};
	};
</script>
 -->
<!-- status select option -->
<script>
	$(".select-color").on('change', function () {
		var color = $("option:selected", this).attr("id");
		$(this).attr("id", color);
	}).change();
</script>
<!-- status select option -->
<!-- clickable dropdown -->
<script>
	$(function () {
		var Accordion = function (el, multiple) {
			this.el = el || {};
			this.multiple = multiple || false;

            // Variables privadas
            var links = this.el.find('.link');
            // Evento
            links.on('click', { el: this.el, multiple: this.multiple }, this.dropdown)
        }

        Accordion.prototype.dropdown = function (e) {
        	var $el = e.data.el;
        	$this = $(this),
        	$next = $this.next();

        	$next.slideToggle();
        	$this.parent().toggleClass('open, sidemenu-active');

        	if (!e.data.multiple) {
        		$el.find('.submenu').not($next).slideUp().parent().removeClass('open, sidemenu-active');
        	};
        }

        var accordion = new Accordion($('#accordion'), false);
    });

</script>


<!-- end clickable dropdown -->

<script type="text/javascript" src="https://cdn.datatables.net/v/bs4/dt-1.10.20/datatables.min.js"></script>

<script src="<?php echo base_url()?>assets/js/data_tablejs.js"></script>
<script>
	$(document).ready(function(){
		$('.timepicker').mdtimepicker();
	});
</script>
<script>
	$(document).ready(function () {
		$('#table1').DataTable();
	});

</script>	
<script>
		$(document).ready(function () {
			$('#table1').DataTable();
			$('#table2').DataTable();
			$('#table3').DataTable();

		});

	</script>
	<script>

		$(document).ready(function () {
			if (!$('#tabs li a:not(:first)').hasClass('inactive')
			) {
				$('#tabs li a:not(:first)').addClass('inactive');
			}
			$('.tab-data-container').hide();
			$('.tab-data-container:first').show();

			$('#tabs li a').click(function () {
				var t = $(this).attr('id');
				if ($(this).hasClass('inactive')) { //this is the start of our condition 
					$('#tabs li a').addClass('inactive');
					$(this).removeClass('inactive');

					$('.tab-data-container').hide();
					$('#' + t + '-data').fadeIn('slow');
				}
			});
		});
	</script>
	<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=places&key=AIzaSyBxAx4g5NCIpyHIP6s_n20flYKX4Y1CWsk"></script>

<script>
	var searchInput = 'address';

	$(document).ready(function() {
		var autocomplete;
		autocomplete = new google.maps.places.Autocomplete((document.getElementById(searchInput)), {
			types: ['geocode'],
		});

		google.maps.event.addListener(autocomplete, 'place_changed', function() {
			var near_place = autocomplete.getPlace();
			document.getElementById('latitude').value = near_place.geometry.location.lat();
			document.getElementById('longitude').value = near_place.geometry.location.lng();
			calculateDistance();

		});

	});

	//   var endSearchInput = 'address2';

	//   $(document).ready(function() {
	//     var autocomplete;
	//     autocomplete = new google.maps.places.Autocomplete((document.getElementById(endSearchInput)), {
	//       types: ['geocode'],
	//     });

	//     google.maps.event.addListener(autocomplete, 'place_changed', function() {
	//       var near_place = autocomplete.getPlace();
	//       document.getElementById('lat2').value = near_place.geometry.location.lat();
	//       document.getElementById('long2').value = near_place.geometry.location.lng();

	//       calculateDistance();
	//     });
	//   });
</script>