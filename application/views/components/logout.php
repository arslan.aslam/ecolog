<!--logout  modal -->
	<div class="modal fade" id="reject-request">
		<div class="modal-dialog">
			<div class="modal-content modal-field">
				<div class="modal-header">
					<h4 class="text-red text-20 text-weight-600">Logout</h4>
				</div>
				<!-- <form action="login.html" method="post" accept-charset="utf-8"> -->

				<div class="modal-body">
					<p class="text-light-grey">
						Are you sure you want to logout?
					</p><br>

				</div>
				<div class="modal-footer">
					<a class="text-14 btn btn-reject" data-dismiss="modal">NO</a>

					<a href="<?php echo base_url('logout') ?>" type="submit" id="btn-green" class="btn btn-green">YES</a>
				</div>
				<!-- </form> -->
			</div>
		</div>
	</div>