<head>
	<title>EcoLog</title>
	<link rel="shortcut icon" type="image/png" href="<?php echo base_url() ?>assets/images/dummy/180-by-120.png" />
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link href="<?php echo base_url() ?>assets/css/bootstrap.min.css" rel="stylesheet">
	<link href="<?php echo base_url() ?>assets/css/home.css" rel="stylesheet">
	<link href="<?php echo base_url() ?>assets/css/style.css" rel="stylesheet">
	<link href="<?php echo base_url() ?>assets/css/index.css" rel="stylesheet">
	<link href="<?php echo base_url() ?>assets/css/responsive.css" rel="stylesheet">
	<link rel="stylesheet" href="<?php echo base_url() ?>assets/css/jquery.datetimepicker.css">

	<link rel="stylesheet" href="<?php echo base_url() ?>assets/css/custom_datatable.css">
	<link rel="stylesheet" href="<?php echo base_url() ?>assets/css/dataTables.bootstrap4.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/css/notification.css?v=1">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.5.1/chosen.min.css">

	<!-- fontawesome icon link -->
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<!-- font family link -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900&display=swap" rel="stylesheet">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
	<!-- js links -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	<!-- date picker links -->
	<link rel="stylesheet" href="<?php echo base_url() ?>assets/css/datepicker.css">
	<script src="<?php echo base_url() ?>assets/js/main.js"></script>
	<!-- <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script> -->
	<!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js"></script> -->
	<script src="<?php echo base_url() ?>assets/js/datepicker.js"></script>
	<!-- time picker links -->
	<link href="https://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url() ?>assets/css/mdtimepicker.css" rel="stylesheet" type="text/css">
	<script src="<?php echo base_url() ?>assets/js/mdtimepicker.js"></script>
	<!-- modal links -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
	<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script> -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
	<!-- editor links -->
	<script src="<?php echo base_url() ?>assets/tinymce/tinymce.min.js"></script>
	<script src="<?php echo base_url() ?>assets/js/jquery.datetimepicker.full.min.js"></script>

	<script src="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.5.1/chosen.jquery.min.js"></script>

</head>

<body>
	<nav class="navbar--modifier navbar-expand navbar-light">
		<span class="Hamburger">
			<span class="line1"></span>
			<span class="line2"></span>
			<span class="line3"></span>
		</span>
	<!-- header-------------------------- -->
	 <a class="navbar-brand  user-profile-img--modifier"  href="#">
            <img src="<?php echo base_url().'assets/images/dummy/eco.png'?>">
        </a>

        <ul class="navbar-nav ml-auto my-auto pr-4">
            <li class="list-inline-item action-menu-dropdown">
                <div class="user-profile-img">
                    <a href="#" class="">
                        <img src="<?php echo base_url().'assets/images/img-profile.png' ?>"> RexSolution
                    </a>
                    <!-- <ul class="list-style-none action-menu user-block sm-dropdown">
                        <li class="">
                            <a class="text-blackish-light text-14" data-toggle="modal"
                                data-target="#reject-request">Logout</a>
                        </li>
                    </ul> -->
                </div>
            </li>
        </ul>

    </nav>
