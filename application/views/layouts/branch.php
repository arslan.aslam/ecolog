<?php include("header.php"); ?>
    <!--Body-->
<?php $this->load->view('components/branchSidemenu') ?>
    <div class="container-fluid  main-container">
		<div class="row row-streched">
			<!-- end side bar -->

			<?php
			if (isset($_view) && $_view)
				$this->load->view($_view);
			?>

		</div>
	</div>
<?php $this->load->view('components/logout') ?>
<?php $this->load->view('components/notification')?>
<?php $this->load->view('components/js')?>
</body>

</html>