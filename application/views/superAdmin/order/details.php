			<div class="col-12">
                <div class="container-fluid dashboard-content-padding">
                   <div class="row row-space dashboard-head-row ">
                      <div class="pull-left col-12 col-xs-12 col-sm-12 col-md-12 col-lg-12 col-space text-center ">
                         <div class="head">
                            <h2 class="text-blackish">Order Details</h2>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container-fluid dashboard-container dashboard-container-driver--modifier bg-white dashboard-content-padding">
               <div class="row content-body-row custom-row-container mb-4">
                  
                  <div class="pull-left col-12 col-lg-6 col-space content">
                     <div class="row">
                        <div class="col-12">
                            <div class="label-heading-driver">
                                <h4>
                                    Driver Name
                                </h4>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="driver">
                                <span class="driver-detail ">
                                    <img class="img__icon" src="../../assets/images/img-profile.png" alt="" srcset="">
                                </span>
                                <span class="driver-name">John Doe</span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <div class="label-heading-driver">
                                <h4>
                                    Address
                                </h4>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="row">
                                
                                <div class="col-12">
                                    <span class="image"><img src="../../assets/images/red-point.png" alt=""> Ferozpur Road</span>
                                </div>
                            </div>
                            <div class="row mt-2">
                                <div class="col-12">
                                    <div class="image">
                                        <span class="image"><img src="../../assets/images/yellow-point.png" alt=""> Kalma Chowk to Shama</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <!--<div class="pull-left col-2 col-xs-2 col-sm-2 col-md-2 col-lg-2 col-space content "></div>-->
                <!--2nd col-->
                <div class="pull-left col-12  col-lg-6  col-space content text-center my-auto">
                    <div class="row">
                        <div class="col-12 text-left">
                            <div class="label-heading-driver">
                                <h4>
                                    Inventary
                                </h4>
                            </div>
                        </div>
                    </div>
                    <div class="container-fluid  dashboard-container bg-white dashboard-content-padding title-route-heading">
                        
                        <div class="row row-space">
                           <div class="col-12">
                            <div class="table-container tabcontent show-tab" id="add-driver">
                               <div class="table-responsive">
                                  <table class="table table-hover">
                                     <thead class="table-head">
                                        <tr>
                                           
                                           <th>Product</th>
                                           <th>Quantity</th>
																					<!-- <th>Edit</th>
                                                                                       <th>Remove</th> -->
                                                                                   </tr>
                                                                               </thead>
                                                                               <tbody class="table-body">
                                                                                <tr>
                                                                                   
                                                                                   <td><span class="text-light-pink text-12">#236 &nbsp;</span>A5-Jordan-!B</td></td>
                                                                                   <td>2</td>
                                                                               </tr>
                                                                               
                                                                               <tr>
                                                                                   
                                                                                   <td><span class="text-light-pink text-12">#226 &nbsp;</span>A5-Jordan-11B</td></td>
                                                                                   <td>2</td>
                                                                               </tr>
                                                                               
                                                                           </tbody>
                                                                       </table>
                                                                   </div>
                                                               </div>
                                                           </div>
                                                       </div>
                                                       
                                                   </div>
                                               </div>
                                           </div>
                                           
                                       </div>
                                   </div>
                               </div>
                           </div>
                           <!--Driver model-->
                           <div class="modal fade bd-example-modal-lg"  tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true" id="assign-driver">
                              <div class="modal-dialog modal-md" role="document">
                                 <div class="modal-content modal-field">
                                    <div class="modal-header border-bottom">
                                       <h4 class="text-red text-20 text-weight-600">Assign to driver</h4>
                                   </div>
                                   <div class="modal-body modal-body--modifier modal-md pt-0">
                                    <a class="model-anchor"   href="#">
                                        <div class="row mt-4">
                                            <div class="col-12">
                                                <div class="row border-bottom">
                                                    <div class="col-10 pr-0">
                                                        <div class="driver driver-modifier--model">
                                                            <span class="driver-detail pr-2">
                                                                <img class="img__icon" src="../../assets/images/driver5.jpg" alt="" srcset="">
                                                            </span>John Doe
                                                            
                                                        </div>
                                                    </div>
                                                    <div class="col-2 my-auto">
                                                     <div class="round">
                                                         <input type="checkbox" id="checkbox" />
                                                         <label for="checkbox"></label>
                                                     </div>
                                                 </div>
                                             </div>
                                         </div>
                                     </div>
                                 </a>
                                 <a class="model-anchor" href="#">
                                    <div class="row mt-4">
                                        <div class="col-12">
                                            <div class="row border-bottom">
                                                <div class="col-10 pr-0">
                                                    <div class="driver driver-modifier--model">
                                                        <span class="driver-detail pr-2">
                                                            <img class="img__icon" src="../../assets/images/driver4.jpg" alt="" srcset="">
                                                        </span>John Doe
                                                        
                                                    </div>
                                                </div>
                                                <div class="col-2 my-auto">
                                                  <div class="round">
                                                    <input type="checkbox" id="checkbox" />
                                                    <label for="checkbox"></label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                            <a class="model-anchor" href="#">
                                <div class="row mt-4">
                                    <div class="col-12">
                                        <div class="row border-bottom">
                                           <div class="col-10 pr-0">
                                            <div class="driver driver-modifier--model">
                                                <span class="driver-detail pr-2">
                                                    <img class="img__icon" src="../../assets/images/driver 3.jpg" alt="" srcset="">
                                                </span>John Doe
                                                
                                            </div>
                                        </div>
                                        <div class="col-2 my-auto">
                                          <div class="round">
                                            <input type="checkbox" id="checkbox" />
                                            <label for="checkbox"></label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                    <a class="model-anchor" href="#">
                        <div class="row mt-4 border-bottom">
                            <div class="col-12">
                                <div class="row">
                                   <div class="col-10 pr-0">
                                    <div class="driver driver-modifier--model">
                                        <span class="driver-detail pr-2">
                                            <img class="img__icon" src="../../assets/images/driver1.jpg" alt="" srcset="">
                                        </span>John Doe
                                        
                                    </div>
                                </div>
                                <div class="col-2 my-auto">
                                  <div class="round">
                                    <input type="checkbox" id="checkbox" />
                                    <label for="checkbox"></label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </a>
            <a class="model-anchor" href="#">
                <div class="row mt-4 border-bottom">
                    <div class="col-12">
                        <div class="row">
                            <div class="col-10 pr-0">
                                <div class="driver driver-modifier--model">
                                    <span class="driver-detail pr-2">
                                        <img class="img__icon" src="../../assets/images/driver2.jpg" alt="" srcset="">
                                    </span>John Doe
                                    
                                </div>
                            </div>
                            <div class="col-2 my-auto">
                              <div class="round">
                                <input type="checkbox" id="checkbox" />
                                <label for="checkbox"></label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </a>
        <a class="model-anchor" href="#">
            <div class="row mt-4 border-bottom">
                <div class="col-12">
                    <div class="row">
                       <div class="col-10 pr-0">
                        <div class="driver driver-modifier--model">
                            <span class="driver-detail pr-2">
                                <img class="img__icon" src="../../assets/images/driver4.jpg" alt="" srcset="">
                            </span>John Doe
                            
                        </div>
                    </div>
                    <div class="col-2 my-auto">
                      <div class="round">
                        <input type="checkbox" id="checkbox" />
                        <label for="checkbox"></label>
                    </div>
                </div>
            </div>
        </div>
    </div>
</a>


</div>
<div class="modal-footer justify-content-center">
    <!-- <button type="button" class="btn btn-primary btn-lg btn-block">Block level button</button> -->
    <a class="text-14 btn btn-reject" data-dismiss="modal">Close</a>
    <a href="#" type="submit" id="btn-green" class="btn btn-green">Done</a>
</div>
</div>
</div>
</div>