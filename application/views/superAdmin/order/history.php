			<div class="col-12">
                <div class="container-fluid dashboard-content-padding">
					<div class="row row-space dashboard-head-row ">
						<div class="pull-left col-12 col-xs-12 col-sm-12 col-md-12 col-lg-12 col-space text-center ">
							<div class="head">
								<h2 class="text-blackish">Orders</h2>
							</div>
						</div>
					</div>
				</div>
				<div class="container-fluid dashboard-container bg-white dashboard-content-padding">
				
					<div class="row content-head-row bottom-border">
						<div class="pull-left col-12 col-sm-6  head-col">

							<div class="tabs-row shadow-wrapper">
								<ul id="tabs">
									<li><a id="tab-current" >On the way</a></li>
									<li><a id="tab-upcoming" class="inactive">Upcoming</a></li>
									<li><a id="tab-completed" class="inactive">Completed</a></li>
								</ul>
							</div>
						</div>
						<div class="pull-left col-12 col-sm-6 col-space table-head-info text-right">
							<div class="container-fluid">
								<ul class="list-style-none content-tabs ">
									<!-- <a href="add-order.html" class="btn btn-invite" >Add new
                                        + </a> -->

								</ul>
							</div>
						</div>

					</div>
					<div class="row content-body-row">
						<div class="pull-left col-12 col-xs-12 col-sm-12 col-md-12 col-lg-12  content">
							<!-- ************************Feedback**************************** -->
							<div class=" tab-data-container " id="tab-current-data">
								<div class="row content-body-row">
									<div class="pull-left col-12 col-xs-12 col-sm-12 col-md-12 col-lg-12">
										<div class="table-container tabcontent show-tab" id="add-driver">
											<div class="table-responsive">
												<table class="table table-hover" id="table3">
													<thead class="table-head">
														<tr>
															<th>Order From</th>
															<th>
																Order To
															</th>
															<th>Date/Time</th>
															<th>View</th>
															<!-- <th>View</th>
															<th>Edit</th>
															<th>Remove</th> -->
														</tr>
													</thead>
													<tbody class="table-body">
														<tr>
															
															<td>
																<!-- <span class="svg-block pr-2">
																	<img class="img__icon" src="../../assets/images/img-profile.png" alt="" srcset="">
																</span>
																John Doe -->
																<p>John Doe</p>
															</td>
															<td >
																<!-- <p class="task__title">Lorem ipsum dolor sit amet.</p>
																<div class="task__div images-task">
																	<p class="task__heading"><img src="../../assets/images/dummy/pin.png" alt=""> Ferozpur Road</p>
																	<p class="task__sub"><img src="../../assets/images/dummy/pin.png" alt=""> Kalma Chowk to Shama</p>
																</div> -->
															<p>John Ana</p>
			
																
															</td>
															<td>24/10/2020: 10:50pm</td>
															
															 <td>
																<a href="<?php echo base_url().'Orders/orderDetails' ?>">
																	<div class="table-icon text-left">
																		<div class="svg-block">
																			<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svgjs="http://svgjs.com/svgjs" version="1.1" width="21" height="21" x="0" y="0" viewBox="0 0 469.333 469.333" style="enable-background:new 0 0 512 512" xml:space="preserve" class=""><g>
																			<g xmlns="http://www.w3.org/2000/svg">
																				<g>
																					<g>
																						<path d="M234.667,170.667c-35.307,0-64,28.693-64,64s28.693,64,64,64s64-28.693,64-64S269.973,170.667,234.667,170.667z" fill="#a19d9d" data-original="#000000" style="" class=""/>
																						<path d="M234.667,74.667C128,74.667,36.907,141.013,0,234.667c36.907,93.653,128,160,234.667,160     c106.773,0,197.76-66.347,234.667-160C432.427,141.013,341.44,74.667,234.667,74.667z M234.667,341.333     c-58.88,0-106.667-47.787-106.667-106.667S175.787,128,234.667,128s106.667,47.787,106.667,106.667     S293.547,341.333,234.667,341.333z" fill="#a19d9d" data-original="#000000" style="" class=""/>
																					</g>
																				</g>
																			</g>
																			<g xmlns="http://www.w3.org/2000/svg">
																			</g>
																			<g xmlns="http://www.w3.org/2000/svg">
																			</g>
																			<g xmlns="http://www.w3.org/2000/svg">
																			</g>
																			<g xmlns="http://www.w3.org/2000/svg">
																			</g>
																			<g xmlns="http://www.w3.org/2000/svg">
																			</g>
																			<g xmlns="http://www.w3.org/2000/svg">
																			</g>
																			<g xmlns="http://www.w3.org/2000/svg">
																			</g>
																			<g xmlns="http://www.w3.org/2000/svg">
																			</g>
																			<g xmlns="http://www.w3.org/2000/svg">
																			</g>
																			<g xmlns="http://www.w3.org/2000/svg">
																			</g>
																			<g xmlns="http://www.w3.org/2000/svg">
																			</g>
																			<g xmlns="http://www.w3.org/2000/svg">
																			</g>
																			<g xmlns="http://www.w3.org/2000/svg">
																			</g>
																			<g xmlns="http://www.w3.org/2000/svg">
																			</g>
																			<g xmlns="http://www.w3.org/2000/svg">
																			</g>
																			</g></svg>
																		</div>
																	</div>
																</a>
															</td>
															
														</tr>
														
														
			
													</tbody>
												</table>
											</div>
										</div>
			
			
			
									</div>
			
								</div>
							</div>
							
							

							<!-- ************************Feedback**************************** -->
							<div class="tab-data-container " id="tab-upcoming-data">

								<div class="row content-body-row">
									<div class="pull-left col-12 col-xs-12 col-sm-12 col-md-12 col-lg-12 ">
										<div class="table-container tabcontent show-tab" id="add-driver">
											<div class="table-responsive">
												<table class="table table-hover" id="table2">
													<thead class="table-head">
														<tr>
															<th>Order From</th>
															<th>
																Order To
															</th>
															<th>Date/Time</th>
															<th>View</th>
															<!-- <th>View</th>
															<th>Edit</th>
															<th>Remove</th> -->
														</tr>
													</thead>
													<tbody class="table-body">
														<tr>
															
															<td>
																<!-- <span class="svg-block pr-2">
																	<img class="img__icon" src="../../assets/images/img-profile.png" alt="" srcset="">
																</span>
																John Doe -->
																<p>Hohn Filip</p>
															</td>
															<td>
																<!-- <p class="task__title">Lorem ipsum dolor sit amet.</p>
																<div class="task__div images-task">
																	<p class="task__heading"><img src="../../assets/images/dummy/pin.png" alt=""> Ferozpur Road</p>
																	<p class="task__sub"><img src="../../assets/images/dummy/pin.png" alt=""> Kalma Chowk to Shama</p>
																</div> -->
																<p>Ana Roberts</p>																
															</td>
															<td>24/10/2020: 10:50pm</td>
															
															<td>
																<a href="<?php echo base_url().'Orders/orderDetails' ?>">
																	<div class="table-icon text-left">
																		<div class="svg-block">
																			<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svgjs="http://svgjs.com/svgjs" version="1.1" width="21" height="21" x="0" y="0" viewBox="0 0 469.333 469.333" style="enable-background:new 0 0 512 512" xml:space="preserve" class=""><g>
																			<g xmlns="http://www.w3.org/2000/svg">
																				<g>
																					<g>
																						<path d="M234.667,170.667c-35.307,0-64,28.693-64,64s28.693,64,64,64s64-28.693,64-64S269.973,170.667,234.667,170.667z" fill="#a19d9d" data-original="#000000" style="" class=""/>
																						<path d="M234.667,74.667C128,74.667,36.907,141.013,0,234.667c36.907,93.653,128,160,234.667,160     c106.773,0,197.76-66.347,234.667-160C432.427,141.013,341.44,74.667,234.667,74.667z M234.667,341.333     c-58.88,0-106.667-47.787-106.667-106.667S175.787,128,234.667,128s106.667,47.787,106.667,106.667     S293.547,341.333,234.667,341.333z" fill="#a19d9d" data-original="#000000" style="" class=""/>
																					</g>
																				</g>
																			</g>
																			<g xmlns="http://www.w3.org/2000/svg">
																			</g>
																			<g xmlns="http://www.w3.org/2000/svg">
																			</g>
																			<g xmlns="http://www.w3.org/2000/svg">
																			</g>
																			<g xmlns="http://www.w3.org/2000/svg">
																			</g>
																			<g xmlns="http://www.w3.org/2000/svg">
																			</g>
																			<g xmlns="http://www.w3.org/2000/svg">
																			</g>
																			<g xmlns="http://www.w3.org/2000/svg">
																			</g>
																			<g xmlns="http://www.w3.org/2000/svg">
																			</g>
																			<g xmlns="http://www.w3.org/2000/svg">
																			</g>
																			<g xmlns="http://www.w3.org/2000/svg">
																			</g>
																			<g xmlns="http://www.w3.org/2000/svg">
																			</g>
																			<g xmlns="http://www.w3.org/2000/svg">
																			</g>
																			<g xmlns="http://www.w3.org/2000/svg">
																			</g>
																			<g xmlns="http://www.w3.org/2000/svg">
																			</g>
																			<g xmlns="http://www.w3.org/2000/svg">
																			</g>
																			</g></svg>
																		</div>
																	</div>
																</a>
															</td>
															
														</tr>
														
														
			
													</tbody>
												</table>
											</div>
										</div>
			
			
			
									</div>
			
								</div>
							</div>


							<!-- *******************************Help************************************** -->
							<div class=" tab-data-container " id="tab-completed-data">
								<div class="row content-body-row">
									<div class="pull-left col-12 col-xs-12 col-sm-12 col-md-12 col-lg-12">
										<div class="table-container tabcontent show-tab" id="add-driver">
											<div class="table-responsive">
												<table class="table table-hover" id="table1">
													<thead class="table-head">
														<tr>
															<th>Order From</th>
															<th>
																Order To
															</th>
															<th>Date/Time</th>
															<th>View</th>
															<!-- <th>View</th>
															<th>Edit</th>
															<th>Remove</th> -->
														</tr>
													</thead>
													<tbody class="table-body">
														<tr>
															
															<td class="w-30">
																<!-- <span class="svg-block pr-2">
																	<img class="img__icon" src="../../assets/images/img-profile.png" alt="" srcset="">
																</span> -->
																<p>Doec zing</p> </td>
															<td class="w-25">
																<p>Laibaepe</p>
																<!-- <div class="task__div images-task">
																	<p class="task__heading"><img src="../../assets/images/dummy/pin.png" alt=""> Ferozpur Road</p>
																	<p class="task__sub"><img src="../../assets/images/dummy/pin.png" alt=""> Kalma Chowk to Shama</p>
																</div> -->
															
			
																
															</td>
															<td>24/10/2020: 10:50pm</td>
															<td>
																<a href="<?php echo base_url().'Orders/orderDetails' ?>">
																	<div class="table-icon text-left">
																		<div class="svg-block">
																			<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svgjs="http://svgjs.com/svgjs" version="1.1" width="21" height="21" x="0" y="0" viewBox="0 0 469.333 469.333" style="enable-background:new 0 0 512 512" xml:space="preserve" class=""><g>
																			<g xmlns="http://www.w3.org/2000/svg">
																				<g>
																					<g>
																						<path d="M234.667,170.667c-35.307,0-64,28.693-64,64s28.693,64,64,64s64-28.693,64-64S269.973,170.667,234.667,170.667z" fill="#a19d9d" data-original="#000000" style="" class=""/>
																						<path d="M234.667,74.667C128,74.667,36.907,141.013,0,234.667c36.907,93.653,128,160,234.667,160     c106.773,0,197.76-66.347,234.667-160C432.427,141.013,341.44,74.667,234.667,74.667z M234.667,341.333     c-58.88,0-106.667-47.787-106.667-106.667S175.787,128,234.667,128s106.667,47.787,106.667,106.667     S293.547,341.333,234.667,341.333z" fill="#a19d9d" data-original="#000000" style="" class=""/>
																					</g>
																				</g>
																			</g>
																			<g xmlns="http://www.w3.org/2000/svg">
																			</g>
																			<g xmlns="http://www.w3.org/2000/svg">
																			</g>
																			<g xmlns="http://www.w3.org/2000/svg">
																			</g>
																			<g xmlns="http://www.w3.org/2000/svg">
																			</g>
																			<g xmlns="http://www.w3.org/2000/svg">
																			</g>
																			<g xmlns="http://www.w3.org/2000/svg">
																			</g>
																			<g xmlns="http://www.w3.org/2000/svg">
																			</g>
																			<g xmlns="http://www.w3.org/2000/svg">
																			</g>
																			<g xmlns="http://www.w3.org/2000/svg">
																			</g>
																			<g xmlns="http://www.w3.org/2000/svg">
																			</g>
																			<g xmlns="http://www.w3.org/2000/svg">
																			</g>
																			<g xmlns="http://www.w3.org/2000/svg">
																			</g>
																			<g xmlns="http://www.w3.org/2000/svg">
																			</g>
																			<g xmlns="http://www.w3.org/2000/svg">
																			</g>
																			<g xmlns="http://www.w3.org/2000/svg">
																			</g>
																			</g></svg>
																		</div>
																	</div>
																</a>
															</td>
														</tr>
														
														
			
													</tbody>
												</table>
											</div>
										</div>
			
			
			
									</div>
			
								</div>
				</div>
            </div>
		</div>
    </div>