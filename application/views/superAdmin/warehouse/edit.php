<div class="col-12">
	<div class="container-fluid dashboard-content-padding">
		<div class="row row-space dashboard-head-row ">
			<div class="pull-left col-12 col-xs-12 col-sm-12 col-md-12 col-lg-12 col-space text-center ">

				<div class="head">
					<h2 class="text-blackish">Edit <?php echo  $user['userType'] == manager ? 'Manager' : 'Warehouse'; ?></h2>
				</div>

			</div>
		</div>
	</div>
	<div class="container-fluid dashboard-container-new bg-white dashboard-content-padding">

		<div class="row content-body-row">
			<div class="pull-left col-12 col-xs-12 col-sm-12 col-md-12 col-lg-12 col-space table-content">

				<?php echo form_open('edit/' . $user['id']) ?>

				<div class="row row-space">
					<div class="pull-left col-12 col-xs-12 col-sm-6 col-md-6 col-lg-6 col-space">
						<div class="error-msg"><?php echo form_error('firstName'); ?> </div> <!-- this div for error message -->
						<div class="block-fieldset input-margin">
							<fieldset class="fieldset-border">
								<legend class="fieldset-border">First Name</legend>
								<input type="text" class="form-input" name="firstName" placeholder="" value="<?php echo ($this->input->post('firstName') ? $this->input->post('firstName') : $user['firstName']); ?>" />
							</fieldset>
						</div>
					</div>

					<div class="pull-left col-12 col-xs-12 col-sm-6 col-md-6 col-lg-6 col-space">
						<div class="error-msg"><?php echo form_error('lastName'); ?> </div> <!-- this div for error message -->
						<div class="block-fieldset input-margin">
							<fieldset class="fieldset-border">
								<legend class="fieldset-border">Last Name</legend>
								<input type="text" class="form-input" name="lastName" placeholder="" value="<?php echo ($this->input->post('lastName') ? $this->input->post('lastName') : $user['lastName']); ?>" />
							</fieldset>

						</div>
					</div>
				</div>

				<div class="row row-space">
					<div class="pull-left col-12 col-xs-12 col-sm-6 col-md-6 col-lg-6 col-space">
						<div class="error-msg"><?php echo form_error('phone'); ?> </div> <!-- this div for error message -->
						<div class="block-fieldset input-margin">
							<fieldset class="fieldset-border">
								<legend class="fieldset-border">Phone</legend>
								<input type="text" class="form-input" name="phone" placeholder="" value="<?php echo ($this->input->post('phone') ? $this->input->post('phone') : $user['phone']); ?>" />
							</fieldset>
						</div>
					</div>
					

						<div class="pull-left col-12 col-xs-12 col-sm-6 col-md-6 col-lg-6 col-space">
							<div class="error-msg"><?php echo form_error('address'); ?> </div> <!-- this div for error message -->
							<div class="block-fieldset input-margin">
								<fieldset class="fieldset-border">
									<legend class="fieldset-border">Address</legend>
									<input type="text" class="form-input" name="address" placeholder="" value="<?php echo ($this->input->post('address') ? $this->input->post('address') : $user['address']); ?>" />
								</fieldset>
							</div>
						</div>
					</div>


				<input type="hidden" name="userType" value="<?php echo $user['userType']; ?>" />
				<div class="row row-space text-center">
					<div class="pull-left col-12 col-xs-12 col-sm-12 col-md-12 col-lg-12 col-space">
						<button type="submit" class="btn-filed text-center btn-space btn btn-blue-lg btn-hover" >Save
						</button>

					</div>
				</div>

				<?php echo form_close() ?>



			</div>

		</div>
	</div>
</div>