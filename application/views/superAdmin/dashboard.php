
			<div class="col-12">
                <div class="container-fluid padding-0  analytic-container">
					<div class="row row-space anlytics-row ">
						<div class="pull-left col-3 col-xs-3 col-sm-4 col-md-4 col-lg-4 col-space analytic-head">
							<div class="head">
								<h6 class="text-22 text-weight-500 text-blackish">Stats</h6>
							</div>
						</div>
						<div
							class="pull-left col-9 col-xs-9 col-sm-8 col-md-8 col-lg-8 col-space table-head-info analytic-head">
						</div>
					</div>
					<div class="row row-space anlytics-row blocks-row">
						<div class="pull-left col-6 col-xs-6 col-sm-3 col-md-3 col-lg-3 col-space blocks-col ">
							<div class="row m-0 analytic-block">
								<div class="pull-left col-4 col-xs-4 col-sm-4 col-md-4 col-lg-4 col-space left-block">
									<div class="icon-block vertical-middle-items">
										<img src="<?php echo base_url().'assets/images/warehouse.svg'?>">
									</div>
								</div>
								<div class="pull-left col-8 col-xs-8 col-sm-8 col-md-8 col-lg-8 col-space right-block">
									<div class="info-block vertical-middle-items2">
										<p class="text-15 text-weight-500">Warehouse</p>
										<h6 class="text-18 text-light-pink"><?php echo $totalWarehouse ?></h6>
									</div>
								</div>
								<div class="clearfix"></div>
							</div>
						</div>
						<div class="pull-left col-6 col-xs-6 col-sm-3 col-md-3 col-lg-3 col-space blocks-col ">
							<div class="row m-0 analytic-block">
								<div class="pull-left col-4 col-xs-4 col-sm-4 col-md-4 col-lg-4 col-space left-block">
									<div class="icon-block vertical-middle-items">
										<img src="<?php echo base_url().'assets/images/online-shopping.svg'?>">
									</div>
								</div>
								<div class="pull-left col-8 col-xs-8 col-sm-8 col-md-8 col-lg-8 col-space right-block">
									<div class="info-block vertical-middle-items2">
										<p class="text-15 text-weight-500">Total Orders</p>
										<h6 class="text-18 text-light-pink">24</h6>
									</div>
								</div>
								<div class="clearfix"></div>
							</div>
						</div>
						<div class="pull-left col-6 col-xs-6 col-sm-3 col-md-3 col-lg-3 col-space blocks-col ">
							<div class="row m-0 analytic-block">
								<div class="pull-left col-4 col-xs-4 col-sm-4 col-md-4 col-lg-4 col-space left-block">
									<div class="icon-block vertical-middle-items icon-lg">
										<img src="<?php echo base_url().'assets/images/chauffer.svg'?>">
									</div>
								</div>
								<div class="pull-left col-8 col-xs-8 col-sm-8 col-md-8 col-lg-8 col-space right-block">
									<div class="info-block vertical-middle-items2">
										<p class="text-15 text-weight-500">Drivers </p>
										<h6 class="text-18 text-light-pink"><?php echo $totalDriver?></h6>
									</div>
								</div>
								<div class="clearfix"></div>
							</div>
						</div>
						<div class="pull-left col-6 col-xs-6 col-sm-3 col-md-3 col-lg-3 col-space blocks-col ">
							<div class="row m-0 analytic-block">
								<div class="pull-left col-4 col-xs-4 col-sm-4 col-md-4 col-lg-4 col-space left-block">
									<div class="icon-block vertical-middle-items">
										<img src="<?php echo base_url().'assets/images/project-management.svg'?>">
									</div>
								</div>
								<div class="pull-left col-8 col-xs-8 col-sm-8 col-md-8 col-lg-8 col-space right-block">
									<div class="info-block vertical-middle-items2">
										<p class="text-15 text-weight-500">Manager</p>
										<h6 class="text-18 text-light-pink"><?php echo $totalManager ?></h6>
									</div>
								</div>
								<div class="clearfix"></div>
							</div>
						</div>
					</div>
				</div>
            </div>
		</div>
    </div>