 <div class="col-12">
     <div class="container-fluid dashboard-content-padding">
         <div class="row row-space dashboard-head-row ">
             <div class="pull-left col-12 col-xs-12 col-sm-12 col-md-12 col-lg-12 col-space text-center ">

                 <div class="head">
                     <h2 class="text-blackish"><?php echo  $userType == manager ? 'Managers' : 'Warehouse'; ?></h2>
                 </div>

             </div>
         </div>
     </div>
     <div class="container-fluid dashboard-container bg-white dashboard-content-padding">
         <div class="row content-head-row mb-5">
             <div class="pull-left col-12 col-xs-12 col-sm-4 col-md-4 col-lg-4 col-space head-col">

                 <div class="head">
                     <ul class="list-style-none content-tabs">


                     </ul>
                 </div>


             </div>
             <!-- <div class="pull-left col-12 col-xs-12 col-sm-8 col-md-8 col-lg-8 col-space table-head-info text-right">
                <?php if($userType==warehouse){ ?>
                <div class="container-fluid">
                     <ul class="list-style-none content-tabs ">
                         <?php
                            $path = 'add-manager/' . encode(manager);
                            if ($userType == warehouse) {
                                $path = 'add-warehouse/' . encode(warehouse);
                            }

                            ?>
                         <a href="<?php echo base_url($path) ?>" class="btn btn-invite">Create new
                             + </a>

                     </ul>
                 </div>
             <?php } ?>
             </div> -->
         </div>
         <div class="row content-body-row">
             <div class="pull-left col-12 col-xs-12 col-sm-12 col-md-12 col-lg-12 col-space table-content">
                 <div class="table-container tabcontent show-tab" id="add-driver">
                     <div class="table-responsive">
                         <table class="table table-hover" id="table1">
                             <thead class="table-head">
                                 <tr>
                                     <th>Name</th>
                                     <th style="width: 30%">Email</th>
                                     <td >Phone</td>
                                     <th>Address</th>
                                     <th>Edit</th>
                                     <th class="text-center">Remove</th>
                                 </tr>
                             </thead>
                             <tbody class="table-body">
                                 <?php foreach ($data as $row) : ?>
                                     <tr>
                                         <td><?php echo $row['firstName'] . ' ' . $row['lastName'] ?></td>
                                         <td><?php echo $row['email'] ?></td>
                                         <td><?php echo $row['phone'] ?></td>
                                         <td><?php echo $row['address'] ?></td>
                                         <td>
                                             <a href="<?php echo base_url('edit/' . $row['id']) ?>">
                                                 <div class="table-icon text-left">
                                                     <div class="svg-block">
                                                         <svg class="svg-icon" xmlns="http://www.w3.org/2000/svg" width="21.776" height="21.776" viewBox="0 0 21.776 21.776">
                                                             <g id="create-new-pencil-button" transform="translate(0 0.001)">
                                                                 <g id="create" transform="translate(0 -0.001)">
                                                                     <path id="Path_56" data-name="Path 56" d="M0,17.178v4.6H4.6L17.9,8.347l-4.6-4.6ZM21.413,4.838a1.169,1.169,0,0,0,0-1.694L18.631.362a1.169,1.169,0,0,0-1.694,0L14.76,2.54l4.6,4.6Z" transform="translate(0 0.001)" fill="#9fa9bc"></path>
                                                                 </g>
                                                             </g>
                                                         </svg>
                                                         
                                                     </div>
                                                 </div>
                                             </a>
                                         </td>
                                         <td>
                                             <div class="table-icon text-center">
                                                 <!-- <img src="../../assets/images/img-delete.png"> -->
                                                 <div class="svg-block">
                                                 <a data-toggle="modal"  data-target="#remove-<?php echo $row['id'] ?>">

                                                     <svg class="svg-icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 16.945 23.107">
                                                         <g id="Delete__x2F__Trash" transform="translate(-35.796)">
                                                             <path id="Path_57" data-name="Path 57" d="M38.149,21.539a2.1,2.1,0,0,0,2.116,1.568h8.011a2.1,2.1,0,0,0,2.116-1.568L51.972,6.162h-15.4Zm9.2-13.066a.77.77,0,0,1,1.541,0l-.77,11.554a.77.77,0,1,1-1.54,0Zm-3.851,0a.77.77,0,1,1,1.54,0V20.027a.77.77,0,1,1-1.54,0ZM40.418,7.7a.77.77,0,0,1,.77.77l.77,11.554a.77.77,0,0,1-1.541,0l-.77-11.554A.77.77,0,0,1,40.418,7.7ZM51.51,3.082H48.121V1.541C48.121.372,47.743,0,46.581,0H41.959a1.347,1.347,0,0,0-1.54,1.541V3.082H37.029A1.2,1.2,0,0,0,35.8,4.238a1.2,1.2,0,0,0,1.232,1.156H51.51a1.2,1.2,0,0,0,1.232-1.156A1.2,1.2,0,0,0,51.51,3.082Zm-4.93,0H41.959V1.54h4.621V3.082Z" fill="#e86572" fill-rule="evenodd" />
                                                         </g>
                                                     </svg>
                                                     </a>
                                                 </div>
                                             </div>
                                         </td>
                                     </tr>
                                 <?php endforeach; ?>


                             </tbody>
                         </table>
                     </div>
                 </div>



             </div>

         </div>



     </div>

 </div>
 </div>
 </div>

 <!--Delete  modal -->
 <?php foreach ($data as $c) : ?>

     <div class="modal fade" id="remove-<?php echo $c['id'] ?>">
         <div class="modal-dialog">
             <div class="modal-content modal-field">
                 <div class="modal-header">
                     <h4 class="text-red text-20 text-weight-600">Delete User</h4>
                 </div>
                 <form action="<?php echo base_url('User/remove') ?>" method="post" accept-charset="utf-8">

                     <input type="hidden" name="id" value="<?php echo $c['id'] ?>">
                     <div class="modal-body">
                         <p class="text-light-grey">
                             Do you want to delete this user '<?php echo $c['firstName'].' '.$c['lastName'] ?>' ?
                         </p><br>

                     </div>
                     <div class="modal-footer">
                         <a class="text-14 btn btn-reject" data-dismiss="modal">NO</a>

                         <button type="submit" id="btn-green" class="btn btn-green">YES</button>
                     </div>
                 </form>
             </div>
         </div>
     </div>

 <?php endforeach; ?>





 <script>
     $(document).ready(function() {
         $('#table').DataTable();
     });
 </script>