<?php 
class Product extends CI_Controller
{
  function __construct()
  {
    parent::__construct();
    $this->load->model('CategoryModel');
    $this->load->model('ProductModel');
    $this->load->model('UserModel');
    auth();
  }

  function index()
  {
    $params['userType'] =$this->session->userdata('userType');
    $userId=$this->session->userdata('userId');
    $data['products']=$this->ProductModel->getAllProducts($userId);
    $user = $this->UserModel->getUser($userId);
    if($params['userType']==branch)
    {
      $data['_view'] = 'branch/product/list';
      $this->load->view('layouts/branch', $data);
      return;
    }
    $data['_view'] = 'warehouse/product/list';
    $this->load->view('layouts/warehouse', $data);
  }

  function addProduct()
  {
    $data['category']=$this->CategoryModel->getAllCategory();
    $params['userType'] =decode($this->uri->segment(2));
    $this->form_validation->set_rules('name', 'Product Name', 'required|max_length[50]');
    $this->form_validation->set_rules('code', 'Product Code', 'required|max_length[50]|is_unique[product.code]');
    $this->form_validation->set_rules('quantity', 'Product Quantity', 'required|max_length[50]');
    $this->form_validation->set_rules('category', 'Category', 'required|max_length[50]');
    if (empty($_FILES['image']['name'])) {
      $this->form_validation->set_rules('image', 'Image', 'max_length[255]|required');
    }
    if ($this->form_validation->run()) 
    {
      if (!empty($_FILES['image']['name'])) 
      {

       $config['upload_path'] = $this->config->item('imageUploadPath');
       $config['allowed_types'] = 'jpg|jpeg|png|gif';
       $removeSpaces = preg_replace('/[^A-Za-z0-9]/', "", $_FILES['image']['name']);
       $config['file_name'] = time() . $removeSpaces;
       $config['max_size']  = '0';
       $config['overwrite'] = TRUE;

       $this->load->library('upload', $config);
       $this->upload->initialize($config);

       if ($this->upload->do_upload('image'))
       {
          $image_data = $this->upload->data();
          $image = $image_data['file_name'];
        }
        else
        {
          $this->session->set_flashdata('error', $this->upload->display_errors());
          Redirect('add-product/'.encode(branch));
        }
      }
      $name = $this->input->post('name');
      $code = $this->input->post('code');
      $quantity = $this->input->post('quantity');
      $category = $this->input->post('category');
      $userId = $this->session->userdata('userId');
      $dataToStore = array(
       'name' => $name,
       'code' => $code,
       'quantity' => $quantity,
       'category' => $category,
       'image' => $image,
       'userId' => $userId,
       'dateCreated' => time(),
      );
      if($this->ProductModel->addProduct($dataToStore));
      {
        $this->session->set_flashdata('success', 'Product added successfully.');
        redirect('product-list');
      }
    }
    if($params['userType']==branch)
    {
      $data['_view'] = 'branch/product/add';
      $this->load->view('layouts/branch', $data);
      return;
    }
    $data['_view'] = 'warehouse/product/add';
    $this->load->view('layouts/warehouse', $data);
  }
  function deleteProduct($id)
  {
    if($this->ProductModel->deleteProduct($id))
    {
      $this->session->set_flashdata('success', 'Product deleted successfully.');
      redirect('product-list/'.encode(branch));
    }
  }
}

?>