<?php 
class Category extends CI_Controller
{
  function __construct()
  {
    parent::__construct();
    $this->load->model('CategoryModel');
    auth();
  }

  function index()
  {
  	$data['category']=$this->CategoryModel->getAllCategory();
  	$data['_view'] = 'warehouse/category/list';
    $this->load->view('layouts/warehouse', $data);
  }

  function addCategory()
  {
    if ($this->input->post())
    {
     $this->form_validation->set_rules('title', 'Category Title', 'required|max_length[50]');
     $this->form_validation->set_rules('description', 'Category Description', 'required|max_length[2000]');
     if (empty($_FILES['image']['name'])) {
      $this->form_validation->set_rules('image', 'Image', 'max_length[255]|required');
    }
    if ($this->form_validation->run()) {

      if (!empty($_FILES['image']['name'])) {

       $config['upload_path'] = $this->config->item('imageUploadPath');
       $config['allowed_types'] = 'jpg|jpeg|png|gif';
       $removeSpaces = preg_replace('/[^A-Za-z0-9]/', "", $_FILES['image']['name']);
       $config['file_name'] = time() . $removeSpaces;


       $config['max_size']  = '0';
       $config['overwrite'] = TRUE;

       $this->load->library('upload', $config);
       $this->upload->initialize($config);

       if ($this->upload->do_upload('image'))
       {

        $image_data = $this->upload->data();
        $image = $image_data['file_name'];
      } else {
        $this->session->set_flashdata('error', $this->upload->display_errors());
        Redirect('add-category');}
      }
      $title = $this->input->post('title');
      $description = $this->input->post('description');
      $dataToStore = array(

       'title' => $title,
       'description' => $description,
       'image' => $image,
       'dateCreated' => time(),
     );
      $this->CategoryModel->addCategory($dataToStore);

      $this->session->set_flashdata('success', 'Category Created successfully.');
      redirect('category');
    }
    $data['_view'] = "warehouse/category/add";
    $this->load->view("layouts/warehouse", $data);
    return;
  }
  $data['_view'] = 'warehouse/category/add';
  $this->load->view('layouts/warehouse', $data);
}


function editCategory($categoryId)
{
  $id=decode($categoryId);
  $data=$this->CategoryModel->getCategory($id);
  $data['category']=$data;
  $this->form_validation->set_rules('title', 'Category Title', 'required|max_length[50]');
  $this->form_validation->set_rules('description', 'Category Description', 'required|max_length[2000]');
  if ($this->form_validation->run('form'))
  {
    if (!empty($_FILES['image']['name']))
    {

      $config['upload_path'] = $this->config->item('imageUploadPath');
      $config['allowed_types'] = 'jpg|jpeg|png|gif';
      $removeSpaces = preg_replace('/[^A-Za-z0-9]/', "", $_FILES['image']['name']);
      $config['file_name'] = time() . $removeSpaces;


      $config['max_size']  = '0';
      $config['overwrite'] = TRUE;

      $this->load->library('upload', $config);
      $this->upload->initialize($config);

      if ($this->upload->do_upload('image'))
      {

        $image_data = $this->upload->data();
        $image = $image_data['file_name'];
      } 
      else
      {
        $this->session->set_flashdata('error', $this->upload->display_errors());
        Redirect('edit-category');
      }
    }
    else
    {
      $image=$data['image'];
    }
    $title = $this->input->post('title');
    $description = $this->input->post('description');
    $dataToStore = array(

     'title' => $title,
     'description' => $description,
     'image' => $image,
     'dateCreated' => time(),
   );
    $this->CategoryModel->updateCategory($id,$dataToStore);

    $this->session->set_flashdata('success', 'Category updated successfully.');
    redirect('category');
  }
  $data['_view'] = "warehouse/category/edit";
  $this->load->view("layouts/warehouse", $data);
}
function deleteCategory($id)
{
  if($this->CategoryModel->deleteCategory($id))
  {
    $this->session->set_flashdata('success', 'Category deleted successfully.');
    redirect('category');
  }
}

}

?>