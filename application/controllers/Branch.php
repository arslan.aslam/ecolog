<?php 
class Branch extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('UserModel');
	}

	function index()
	{
		$data['_view'] = 'branch/dashboard';
		$this->load->view('layouts/branch', $data);
	}

	function managers()
	{
		auth();
		$params['userType'] =decode($this->uri->segment(2));
		$params['createdId']=$this->session->userdata('userId');
		$data['data'] = $this->UserModel->getAllUser($params);
		$data['userType'] = $params['userType'];
		$data['_view'] = 'branch/manager/list';
		$this->load->view('layouts/branch', $data);
	}

	function drivers()
	{
		auth();
		$params['userType'] =decode($this->uri->segment(2));
		$params['createdId']=$this->session->userdata('userId');
		$data['data'] = $this->UserModel->getAllUser($params);
		$data['userType'] = $params['userType'];
		$data['_view'] = 'branch/driver/list';
		$this->load->view('layouts/branch', $data);
	}

	function profile()
	{
		auth();
		$params['userType'] =decode($this->uri->segment(2));
		$userId=$this->session->userdata('userId');
		$data['user'] = $this->UserModel->getUser($userId);
		$data['userType'] = $params['userType'];
		$data['_view'] = 'branch/profile';
		$this->load->view('layouts/branch', $data);
	}
}
?>