<?php 
class Orders extends CI_Controller
{
  function __construct()
  {
    parent::__construct();
  }


  function index()
  {
    $data['_view'] = 'superAdmin/order/history';
    $this->load->view('layouts/main', $data);
  }
  function orderDetails()
  {
  	$data['_view'] = 'superAdmin/order/details';
    $this->load->view('layouts/main', $data);
  }
}

 ?>