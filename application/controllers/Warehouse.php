<?php 
class Warehouse extends CI_Controller
{
  function __construct()
  {
    parent::__construct();
    $this->load->Model('UserModel');
    auth();
  }


  function index()
  {
  	warehouseAuth();
    $data['data'] = $this->UserModel->getAllUser();
    $data['_view'] = 'warehouse/branch/search';
    $this->load->view('layouts/warehouse', $data);
  }

  function addWarehouse()
  {

  }
  
  function branches()
  {
    warehouseAuth();
    $params['userType'] = decode($this->uri->segment(2));
    $data['data'] = $this->UserModel->getAllUser($params);
    $data['userType'] = $params['userType'];
    $data['_view'] = 'warehouse/branch/list';
    $this->load->view('layouts/warehouse', $data);
  }

  function profile()
  {
    $params['userType'] =decode($this->uri->segment(2));
    $userId=$this->session->userdata('userId');
    $data['user'] = $this->UserModel->getUser($userId);
    $data['userType'] = $params['userType'];
    $data['_view'] = 'warehouse/profile';
    $this->load->view('layouts/warehouse', $data);
  }
  function managers()
  {
    $params['userType'] =decode($this->uri->segment(2));
    $userId=$this->session->userdata('userId');
    $params['sessionId'] =$userId;
    $data['data'] = $this->UserModel->getAllUser($params);
    $data['user'] = $this->UserModel->get($userId);
    $data['userType'] = $params['userType'];
    $data['_view'] = 'warehouse/manager/list';
    $this->load->view('layouts/warehouse', $data);
  }

   function drivers()
    {
      warehouseAuth();
      $params['userType'] =decode($this->uri->segment(2));
      $data['data'] = $this->UserModel->getAllUser($params);
      $data['userType'] = $params['userType'];
      $data['_view'] = 'warehouse/driver/list';
      $this->load->view('layouts/warehouse', $data);
    }
}
?>