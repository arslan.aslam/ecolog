<?php


class User extends CI_Controller
{
  function __construct()
  {
    parent::__construct();
    $this->load->model('UserModel');
  }

  function index()
  {
    adminAuth();
    $data['totalWarehouse'] = $this->UserModel->getAllUserCount($params['userType']=warehouse);
    $data['totalManager'] = $this->UserModel->getAllUserCount($params['userType']=manager);
    $data['totalDriver'] = $this->UserModel->getAllUserCount($params['userType']=driver);
    $data['_view'] = 'superAdmin/dashboard';
    $this->load->view('layouts/main', $data);
  }
  /*
     * Deleting user
     */
  function remove()
  {
    auth();
    $id = $this->input->post('id');
    $user = $this->UserModel->getUser($id);
    // check if the user exists before trying to delete it
    if (isset($user['id'])) {
      $this->UserModel->deleteUser($id);
      if ($user['userType'] == manager) {
        $this->session->set_flashdata('success', 'Manager deleted successfully.');
        redirect($_SERVER['HTTP_REFERER']);
      }
      elseif ($user['userType']==warehouse) {
        $this->session->set_flashdata('success', 'Warehouse deleted successfully.');
        redirect($_SERVER['HTTP_REFERER']);
      }
      elseif ($user['userType']==branch) {
        $this->session->set_flashdata('success', 'Branch deleted successfully.');
        redirect($_SERVER['HTTP_REFERER']);
      }
      $this->session->set_flashdata('success', 'Driver deleted successfully.');
      redirect($_SERVER['HTTP_REFERER']);
    } else
    show_error('The user you are trying to delete does not exist.');
  }


  function changePassword()
  {
    auth();
    $token = $this->input->post('token');
    $this->load->library('form_validation');
    $this->form_validation->set_rules('password', 'Password', 'required|min_length[6]');
    $this->form_validation->set_rules('confPassword', 'confirm Password', 'required|matches[password]');

    if ($this->form_validation->run()) {

      $token     = $this->input->post('token');
      $result = $this->input->post('result');
      $password   = md5(md5($this->input->post('password')));
      $params = array(
        'password'  =>  $password,
        'passwordToken' => "",
      );


      if (!empty($result)) {
        $forgetPassword = $this->UserModel->updatePasswordWithEmail($token, $params);
      } else {
        $forgetPassword = $this->UserModel->forgetPassword($token, $params);
      }

      if ($forgetPassword) {
        $this->session->set_flashdata('success', 'Password Updated Successfully.');
        redirect('login');


      } else {
        $this->session->set_flashdata('error', 'Token Expired!');
      }
    }
    $data['token'] = $this->input->post('token');
    $data['_view'] = 'registration/updatePassword';
    $this->load->view('layouts/registerTemplate', $data);
    
  }


  // Create and Update Password View 
  function updatePassword($token)
  {

    // registrationAuth();

    $data['result'] = decode($this->uri->segment(4));
    $data['token'] = $token;

    if ($data['result'] == 1) {

      $result = $this->UserModel->checkAlreadyActivation($data['token']);


      if ($result['status'] == 1) {
        echo "<h2><font color='red'>Link Expired!</font></h2>";
        die;
      } else {

        $data['_view'] = 'registration/updatePassword';
        $this->load->view('layouts/registerTemplate', $data);
      }
    } else {
      $exist = $this->UserModel->passwordTokenExist($data['token']);
      if (empty($exist)) {
        echo "<h2><font color='red'>Link Expired!</font></h2>";
        die;
      } else {


        $data['_view'] = 'registration/updatePassword';
        $this->load->view('layouts/registerTemplate', $data);
      }
    }
  }


  public function login()
  {
    registrationAuth();
    if ($this->input->post('submitted') == 'login') {

      $this->form_validation->set_rules('email', 'Email', 'required');
      $this->form_validation->set_rules('password', 'Password', 'required');
      if ($this->form_validation->run()) {
        $email = $this->input->post('email');
        $params['email'] = $email;
        $params['password'] = md5(md5($this->input->post('password')));
        $user   = $this->UserModel->validateCredential($params);
        if($user)
        {
          updateSession($user['id']);
          checkUserType($user);
        }
        $this->session->set_flashdata('error', 'Enter valid Credentials');
      }
    }
    $data['_view'] = "registration/login";
    $this->load->view("layouts/registerTemplate", $data);
  }

  function logout()
  {

    $this->session->sess_destroy();
    redirect('login');
  }


  function changePasswords()
  {
    $userType=decode($this->uri->segment(2));
    if($this->input->post())
    {
      $this->form_validation->set_rules('oldPassword', 'Old Password', 'required|min_length[6]');
      $this->form_validation->set_rules('newPassword', 'Password', 'required|min_length[6]');
      $this->form_validation->set_rules('confPassword', 'confirm Password', 'required|matches[newPassword]');
      if($this->form_validation->run())
      {
        $userId=$this->session->userdata('userId');
        $userObj=$this->UserModel->getUser($userId);
        $temp['password']=md5(md5($this->input->post('oldPassword')));
        $temp['email']=$userObj['email'];
        $params['password']=md5(md5($this->input->post('newPassword')));
        $data=$this->UserModel->validateCredential($temp);
        if($data)
        {
          if($this->UserModel->updateUser($userId,$params))
          {
            $this->session->set_flashdata('success', 'Password Changed successfully!');
            redirect($_SERVER['HTTP_REFERER']);
          }
          $this->session->set_flashdata('error', 'Password not changed successfully try again!');
          redirect($_SERVER['HTTP_REFERER']);
        }
        $this->session->set_flashdata('error', 'Please enter correct old password!');
        redirect($_SERVER['HTTP_REFERER']);
      }
    }
    if($userType==admin)
    {
      $data['_view'] = 'superAdmin/changePassword';
      $this->load->view('layouts/main', $data);}
    elseif($userType==warehouse)
    {
      $data['_view'] = 'warehouse/changePassword';
      $this->load->view('layouts/warehouse', $data);}
    elseif($userType==branch)
    {
      $data['_view'] = 'branch/changePassword';
      $this->load->view('layouts/branch', $data);}
  }

  public function forgotPassword()
  {
    $this->load->library('form_validation');

    registrationAuth();

    $this->form_validation->set_rules('email', 'E-mail', 'required');
    if ($this->form_validation->run()) {

      $params['email'] = $this->input->post('email');
      $result = $this->UserModel->isEmailExist($params);

      if (empty($result)) {
        $this->session->set_flashdata('error', 'Email did not found.');
        redirect('forgot-password');
      }
      if (!empty($result['email'])) {
        $passwordToken = random_string('alnum', 20);
        $array = array('passwordToken' => $passwordToken);
        $subject = "Forgot Password";
        $data['name'] = $result['firstName'].' '.$result['lastName'];
        $data['token'] = $passwordToken;
        $data['email'] = $result['email'];

        $message = $this->load->view('emailTemplates/forgotPasswordLink', $data, true);
        if (sendEmail($params['email'], $subject, $message)) {
          $this->UserModel->updateUser($result['id'], $array);
          $this->session->set_flashdata('success', 'Email sent successfully, Please check email inbox.');
          redirect('forgot-password');
        } else {
          $this->session->set_flashdata('error', 'Email is not sent, Please try again.');
          redirect('forgot-password');
        }
      } else {
        $this->session->set_flashdata('error', 'Oops! Error.Please try again later!');
        redirect('forgot-password');
      }
    }
    $data['_view'] = "registration/forgotPassword";
    $this->load->view("layouts/registerTemplate", $data);
  }

  public function addUser($userType)
  {
    auth();
    $this->form_validation->set_rules('firstName', 'First Name', 'required|max_length[255]');
    $this->form_validation->set_rules('lastName', 'Last Name', 'required|max_length[255]');
    $this->form_validation->set_rules('password', 'Password', 'required|min_length[5]|max_length[50]');
    $this->form_validation->set_rules('confPassword', 'Confirm Password', 'required|matches[password]');
    $this->form_validation->set_rules('email', 'Email', 'required|max_length[255]|is_unique[user.email]|valid_email', array('is_unique' => 'Email is already exits, Please try with different one!'));
    $this->form_validation->set_rules('phone', 'Phone', 'required|max_length[255]|is_unique[user.phone]');
    $this->form_validation->set_rules('address', 'address', 'required|max_length[255]');
    $data['userType'] = decode($this->uri->segment(2));
    $userType = $data['userType'];
     if ($userType == branch) {
      $this->form_validation->set_rules('address', 'Please Enter Valid google Address', 'required|max_length[255]');
    }
    if ($userType == driver) {
      $this->form_validation->set_rules('vehicleNo', 'Vehicle Number', 'required|max_length[255]|is_unique[user.vehicleNo]');
    }
    if ($this->form_validation->run()) {
      $firstName       =   $this->input->post('firstName');
      $lastName       =   $this->input->post('lastName');
      $email       =   $this->input->post('email');
      $phone       =   $this->input->post('phone');
      $address       =   $this->input->post('address');
      $latitude       =   ($this->input->post('latitude')?$this->input->post('latitude'):0);
      $longitude       =   ($this->input->post('longitude')?$this->input->post('longitude'):0);
      $userType       =   $this->input->post('userType');
      $vehicleNo       =   $this->input->post('vehicleNo');
      $user = $this->UserModel->isEmailExist(array('email' => $email));
      $userId = empty($user) ? random_string('alnum', 15) : $user['id'];
      $password = $this->input->post('password');
      $subject = "Invitation";

      $emailParams['firstName'] = empty($user) ? $firstName : $user['firstName'];
      $emailParams['lastName'] = empty($user) ? $lastName : $user['lastName'];
      $emailParams['email'] = empty($user) ? $email : $user['email'];
      $emailParams['password'] = $password;
      $emailParams['userType'] = $userType;
      $message = $this->load->view('emailTemplates/accountCreated', $emailParams, true);
      if($userType == manager){
        $statusMessage='Manager added successfully.';
      }
      elseif($userType == warehouse)
      {
        $statusMessage='Warehouse added successfully.';}
      elseif($userType == driver){
          $statusMessage='Driver added successfully.';}
      elseif($userType == branch){
          $statusMessage='branch added successfully.';}
        if (sendEmail($email, $subject, $message)) {
          if (empty($user)) {

            $dataToStore = array(
              'id' => $userId,
              'id' => $userId,
              'firstName' => $emailParams['firstName'],
              'lastName' => $emailParams['lastName'],
              'createdId' => $this->session->userdata('userId'),
              'email' => $emailParams['email'],
              'phone' => $phone,
              'address' => $address,
              'latitude' => $latitude,
              'longitude' => $longitude,
              'vehicleNo' => $vehicleNo,
              'status' => 1,
              'userType' => $userType,
              'sessionId' => $this->session->userdata('userId'),
              'password' => md5(md5($password)),
              'dateCreated' => time(),
              'dateModified' => time(),
            );
            $this->UserModel->addUser($dataToStore);
            $this->session->set_flashdata('success', $statusMessage);
            if($this->session->userdata('userType')==branch)
            {
              if ($userType == manager) {
              redirect('branch-managers/'.encode(manager));}
              elseif ($userType==driver) {
              redirect('branch-drivers/'.encode(driver));}
            }
            elseif($this->session->userdata('userType')==admin)
            {
              redirect('warehouses/'.encode(warehouse));
            }
            else
            {
              if ($userType == manager) {
              redirect('warehouse-managers/'.encode($userType));}
              elseif ($userType==driver) {
              redirect('drivers/'.encode($userType));}
              elseif ($userType==branch) {
              redirect('branches/'.encode($userType));}
              redirect('warehouses/'.encode($userType));
            }
            
          }

          // $invitationData = array(

          //   'dateCreated' => time(),
          //   'dateModified' => time(),
          // );
          // $this->UserModel->updateUser($userId, $invitationData);
          // if ($userType == manager) {
          //   $this->session->set_flashdata('success', 'Manager updated successfully.');
          //   redirect('managers/'.encode($userType));
          // }
          // elseif ($userType==warehouse) {
          //   $this->session->set_flashdata('success', 'Warehouse updated successfully.');
          //   redirect('warehouses/'.encode($userType));
          // }
          // $this->session->set_flashdata('success', 'Driver updated successfully.');
          // redirect('drivers/'.encode($userType));
        }
      }
       if($this->session->userdata('userType')==admin && $this->session->userdata('isAdmin')!=0)
      {
        $data['_view'] = 'superAdmin/warehouse/add';
        $this->load->view('layouts/main', $data);
        return;
      }
      elseif($this->session->userdata('userType')==branch && $this->session->userdata('isAdmin')==0)
      {
        if($userType==manager)
        {
          $data['_view'] = 'branch/manager/add';}
        elseif($userType==driver)
        {
          $data['_view'] = 'branch/driver/add';}
      $this->load->view('layouts/branch', $data);
      return;
      }
      if($userType==warehouse)
      {
        $data['_view'] = 'warehouse/warehouse/add';}
      elseif($userType==manager)
      {
        $data['_view'] = 'warehouse/manager/add';}
      elseif($userType==driver)
      {
        $data['_view'] = 'warehouse/driver/add';}
      elseif($userType==branch)
      {
        $data['_view'] = 'warehouse/branch/add';}
      $this->load->view('layouts/warehouse', $data);
    }



    public function editUser($userId)
    {
      auth();
    // $userId       =   $this->uri->segment(3);
      $data['user'] = $this->UserModel->getUser($userId);
      $userType       =   $data['user']['userType'];
      $this->form_validation->set_rules('firstName', 'First Name', 'required|max_length[255]');
      $this->form_validation->set_rules('lastName', 'Last Name', 'required|max_length[255]');
      $this->form_validation->set_rules('phone', 'Phone', 'required|max_length[255]');
      $this->form_validation->set_rules('address', 'address', 'required|max_length[255]');
      if ($userType == branch) {
      $this->form_validation->set_rules('address', 'Please Enter valid google Address', 'required|max_length[255]');
        }
      if ($userType == driver) {
        $this->form_validation->set_rules('vehicleNo', 'Vehicle Number', 'required|max_length[255]');
      }
    if ($this->form_validation->run()) {

      if (!empty($_FILES['image']['name'])) {

       $config['upload_path'] = $this->config->item('imageUploadPath');
       $config['allowed_types'] = 'jpg|jpeg|png|gif';
       $removeSpaces = preg_replace('/[^A-Za-z0-9]/', "", $_FILES['image']['name']);
       $config['file_name'] = time() . $removeSpaces;


       $config['max_size']  = '0';
       $config['overwrite'] = TRUE;

       $this->load->library('upload', $config);
       $this->upload->initialize($config);

       if ($this->upload->do_upload('image')) {

        $image_data = $this->upload->data();
        $image = $image_data['file_name'];
      } else {
        $this->session->set_flashdata('error', $this->upload->display_errors());
        Redirect('branch/profile');
      }
    }
     else
      {
        $image=$data['user']['image'];
      }
        if (count($this->input->post()) > 0) {
          $firstName       =   $this->input->post('firstName');
          $lastName       =   $this->input->post('lastName');
          $phone       =   $this->input->post('phone');
          $address       =   $this->input->post('address');
          $latitude       =   $this->input->post('latitude');
          $longitude       =   $this->input->post('longitude');
          $vehicleNo       =   $this->input->post('vehicleNo');

          $dataToStore = array(

            'firstName' => !empty($firstName) ? $firstName : $data['user']['firstName'],
            'lastName' => !empty($lastName) ? $lastName : $data['user']['lastName'],
            'phone' => !empty($phone) ? $phone : $data['user']['phone'],
            'address' => !empty($address) ? $address : $data['user']['address'],
            'latitude' => !empty($latitude) ? $latitude : $data['user']['latitude'],
            'longitude' => !empty($longitude) ? $longitude : $data['user']['longitude'],
            'vehicleNo' => !empty($vehicleNo) ? $vehicleNo : $data['user']['vehicleNo'],
            'image' => !empty($image) ? $image : $data['user']['image'],
            'dateModified' => time(),
          );

          $this->UserModel->updateUser($userId, $dataToStore);

          if($this->session->userdata('userType')==branch)
            {
              if ($userType == manager) {
              $this->session->set_flashdata('success', 'Manager updated successfully.');
              redirect('branch-managers/'.encode(manager));}
              elseif ($userType==driver) {
              $this->session->set_flashdata('success', 'Driver updated successfully.');
              redirect('branch-drivers/'.encode(driver));}
              $this->session->set_flashdata('success', 'Profile updated successfully.');
              redirect('branch-profile/'.encode(branch));
            }
            elseif($this->session->userdata('userType')==admin)
            {
              if ($userType == manager) {
              $this->session->set_flashdata('success', 'Manager updated successfully.');
              redirect('managers/'.encode($userType));}
              elseif($userType == warehouse)
              {
                $this->session->set_flashdata('success', 'Warehouse updated successfully.');
                redirect('warehouses/'.encode(warehouse));
              }
              $this->session->set_flashdata('success', 'Profile updated successfully.');
              redirect('dashboard');
            }
            else
            {
              if ($userType == manager) {
              $this->session->set_flashdata('success', 'Manager updated successfully.');
              redirect('warehouse-managers/'.encode($userType));}
              elseif ($userType==driver) {
              $this->session->set_flashdata('success', 'Driver updated successfully.');
              redirect('drivers/'.encode($userType));}
              elseif ($userType==branch) {
              $this->session->set_flashdata('success', 'Branch updated successfully.');
              redirect('branches/'.encode($userType));}
              $this->session->set_flashdata('success', 'Profile updated successfully.');
              redirect('warehouse-profile/'.encode($userType));
            }
        }
      }
      if($this->session->userdata('userType')==admin && $this->session->userdata('isAdmin')!=0)
      {
        $data['_view'] = 'superAdmin/warehouse/edit';
        $this->load->view('layouts/main', $data);
        return;
      }
      elseif($this->session->userdata('userType')==branch && $this->session->userdata('isAdmin')==0)
      {
        if($userType==manager)
        {
          $data['_view'] = 'branch/manager/edit';}
        elseif($userType==driver)
        {
          $data['_view'] = 'branch/driver/edit';}
        else{
          $data['_view'] = 'branch/profile';}

      $this->load->view('layouts/branch', $data);
      return;
      }
      if($userType==warehouse)
      {
        $data['_view'] = 'warehouse/warehouse/edit';}
      elseif($userType==manager)
      {
        $data['_view'] = 'warehouse/manager/edit';}
      elseif($userType==driver)
      {
        $data['_view'] = 'warehouse/driver/edit';}
      elseif($userType==branch)
      {
        $data['_view'] = 'warehouse/branch/edit';}
      $this->load->view('layouts/warehouse', $data);
    }
    function warehouses()
    {
      auth();
      adminAuth();
      $params['userType'] = decode($this->uri->segment(2));
      $data['data'] = $this->UserModel->getAllUser($params);
      $data['userType'] = $params['userType'];
      $data['_view'] = 'superAdmin/warehouse/list';
      $this->load->view('layouts/main', $data);
    }

    function managers()
    {
      auth();
      $params['userType'] =decode($this->uri->segment(2));
      $data['data'] = $this->UserModel->getAllUser($params);
      $data['userType'] = $params['userType'];
      $data['_view'] = 'superAdmin/manager/list';
      $this->load->view('layouts/main', $data);
    }

    function branches()
    {
      auth();
      $params['userType'] =decode($this->uri->segment(2));
      $data['data'] = $this->UserModel->getAllUser($params);
      $data['userType'] = $params['userType'];
      $data['_view'] = 'superAdmin/branch/list';
      $this->load->view('layouts/main', $data);
    }
    function drivers()
    {
      auth();
      $params['userType'] =decode($this->uri->segment(2));
      $data['data'] = $this->UserModel->getAllUser($params);
      $data['userType'] = $params['userType'];
      $data['_view'] = 'superAdmin/driver/list';
      $this->load->view('layouts/main', $data);
    }
  function profile($userType)
  {
    auth();
    $params['userType'] =decode($this->uri->segment(2));
    $userId=$this->session->userdata('userId');
    $data['user'] = $this->UserModel->getUser($userId);
    $data['userType'] = $params['userType'];
    $data['_view'] = 'superAdmin/profile';
    $this->load->view('layouts/main', $data);
  }
}
