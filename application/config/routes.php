<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['default_controller'] = 'User/login';

$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;


// Registration Routes
$route['login'] = 'User/login';
$route['logout'] = 'User/logout';
$route['update-password/(:any)'] = 'User/updatePassword/$1';	
$route['change-password/(:any)'] = 'User/changePasswords/$1';	
$route['create-password'] = 'User/changePassword';

$route['forgot-password'] = 'User/forgotPassword';

$route['dashboard'] = 'User/index';
$route['branch-dashboard'] = 'Branch/index';
$route['managers/(:any)'] = 'User/managers/$1';
$route['warehouse-managers/(:any)'] = 'Warehouse/managers/$1';
$route['warehouses/(:any)'] = 'User/warehouses/$1';
$route['orders'] = 'Orders/index';
$route['setting'] = 'User/setting';
$route['warehouse']='Warehouse/index';
$route['drivers/(:any)'] = 'Warehouse/drivers/$1';
$route['add-warehouse/(:any)']='User/addUser/$1';
$route['add-manager/(:any)'] = 'User/addUser/$1';
$route['add-driver/(:any)'] = 'User/addUser/$1';
$route['edit/(:any)'] = 'User/editUser/$1';

$route['category']='Category/index';
$route['add-category']='Category/addCategory';
$route['edit-category/(:any)']='Category/editCategory/$1';
$route['delete-category/(:any)']='Category/deleteCategory/$1';

$route['branches/(:any)'] = 'Warehouse/branches/$1';
$route['add-branch/(:any)'] = 'User/addUser/$1';

$route['branch-profile/(:any)'] = 'branch/profile/$1';
$route['warehouse-profile/(:any)'] = 'Warehouse/profile/$1';
$route['superAdmin-profile/(:any)'] = 'User/profile/$1';
$route['branch-managers/(:any)'] = 'branch/managers/$1';
$route['branch-drivers/(:any)'] = 'branch/drivers/$1';

$route['product-list/(:any)'] = 'Product/index/$1';
$route['add-product/(:any)'] = 'Product/addProduct/$1';
// $route['add-task'] = 'Task/addTask';
// $route['edit-task/(:any)'] = 'Task/editTask/$1';
// $route['tasks'] = 'Task/getAllTask';
// $route['add-subtask'] = 'Task/addSubTask';
// $route['assign-managers'] = 'Task/assignManagers/';

// $route['check-point-types'] = 'CheckPoint/getAllCheckPointType';
// $route['add-checkpoint-type'] = 'CheckPoint/addCheckPointType';
// $route['edit-checkpoint-type/(:any)'] = 'CheckPoint/editCheckPointType/$1';

// $route['check-points'] = 'CheckPoint/getAllCheckPoint';
// $route['add-checkpoint'] = 'CheckPoint/addCheckPoint';
// $route['edit-checkpoint/(:any)'] = 'CheckPoint/editCheckPoint/$1';



// $route['tasks'] = 'Task/index/$1';

// $route['edit-subscription/(:any)'] = 'Subscription/edit/$1';


