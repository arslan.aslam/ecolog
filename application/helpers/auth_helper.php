<?php
defined('BASEPATH') or exit('No direct script access allowed');



function auth()
{
	$ci = &get_instance();

	if (empty($ci->session->userdata('userId'))) {
		redirect('login');
	}
}

function registrationAuth()
{

	$ci = &get_instance();

	if (!empty($ci->session->userdata('userId')))
	{
		if ($ci->session->userdata('userType') == admin)
		{
          redirect('dashboard');
        }
        elseif ($ci->session->userdata('userType') == warehouse)
        {
          redirect('warehouse');
      	}
      	elseif ($ci->session->userdata('userType') == branch)
        {
          redirect('branch-dashboard');
      	}
	}
}

function checkUserType($user)
{
	$ci = &get_instance();
 		if ($user['userType'] == 1 && $user['isAdmin'] == 1) {
          $ci->session->set_flashdata('success', 'welcome! login successfully');
          redirect('dashboard');
        } elseif ($user['userType'] == 4 && $user['isAdmin'] == 0) {

          $ci->session->set_flashdata('success', 'welcome! login successfully');
          redirect('warehouse');
        }
        elseif ($user['userType'] == 5 && $user['isAdmin'] == 0) {

          $ci->session->set_flashdata('success', 'welcome! login successfully');
          redirect('branch-dashboard');
        }
        $ci->session->set_flashdata('error', 'Enter valid Credentials');
}

function adminAuth()
{

	$ci = &get_instance();

	if ($ci->session->userdata('userType') != admin) {
		$ci->session->set_flashdata('error', 'Unauthorized Access');
		redirect('warehouse');
	}
}


function warehouseAuth()
{

	$ci = &get_instance();

	if ($ci->session->userdata('userType') != warehouse) {
		$ci->session->set_flashdata('error', 'Unauthorized Access');
		redirect('dashboard');
	}
}



function updateSession($userId)
{
	$ci = &get_instance();
	$ci->load->model('UserModel');
	$user = $ci->UserModel->getUser($userId);
	$sessionData = array(

		'userId'     => $user['id'],
		'email'      => $user['email'],
		'firstName'   => $user['firstName'],
		// 'userType'   => $user['userType'],
		// 'image'      => $user['image'],
		'userType'   => $user['userType'],
		'isAdmin'   => $user['isAdmin'],
		'logged'     => TRUE,
	);
	$ci->session->set_userdata($sessionData);
}

