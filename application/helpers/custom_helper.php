<?php
defined('BASEPATH') or exit('No direct script access allowed');

function uniqueId()
{
	$ci = &get_instance();
	$ci->load->helper('string');
	// $alpha= random_string('alpha', 1);
	return $numeric = random_string('numeric', 3);
	// return $uniqueId= $alpha.'-'.$numeric;

}

function utcTime($dateString)
{
	$date = empty($dateString) ? new DateTime() : new DateTime($dateString);
	//echo "$date".$date->getTimestamp();

	$utc = $date->getTimestamp();
	return $utc;
}
function encode($string)
{
	return urlencode(base64_encode($string));
}

function decode($string)
{
	return base64_decode(urldecode($string));
}


function userModel($userArray)
{

	$ci = &get_instance();
	$ci->load->model(array('UserModel'));


	if (!empty($userArray)) {
		foreach ($userArray as $row) {
	$userImage = getUserImageUrl($row['image']);
			$users[] = array(
				'userId'     => $row['id'],
				'email'      => $row['email'],
				'name'  => $row['name'],
				'userType'   => $row['userType'],
				'vehicleNo'  => $row['vehicleNo'],
				'address'    => $row['address'],
				'isOnline'   => $row['isOnline'],
				'phone'      => $row['phone'],
				'status'     => $row['status'],
				'deviceId'   => $row['deviceId'],
				'deviceToken'=> $row['deviceToken'],
				'image'      => $userImage,
			);
		}
		return $users;
	}

	return [];
}




function getUserImageUrl($image=''){
	$ci = &get_instance();
	if ($image == '') 
	{
		$imageUrl = $ci->config->item('dummy_image_path');
	} 
	else
	{
		$imageUrl = $ci->config->item('image_path') . $image;
	}
	return $imageUrl;
}

function getDateTimeString($unixDate)
{
	$date = date('d-F-Y h:i a', $unixDate);
	return $date;
}


function getAddress($id)
{
	$ci = &get_instance();

	$ci->load->model('CheckPointModel');
	$checkpoint = $ci->CheckPointModel->getCheckpoint($id);
	$result = $checkpoint['address'];
	return $result;
}
