Online JavaScript Beautifier (v1.10.1)
Beautify, unpack or deobfuscate JavaScript and HTML, make JSON/JSONP readable, etc.

All of the source code is completely free and open, available on GitHub under MIT licence, 
and we have a command-line version, python library and a node package as well.



 
 
 

HTML <style>, <script> formatting:


Additional Settings (JSON):

{}
  End script and style with newline? 
 Support e4x/jsx syntax 
 Use comma-first list style? 
 Detect packers and obfuscators? 
 Preserve inline braces/code blocks? 
 Keep array indentation? 
 Break lines on chained methods? 
 Space before conditional: "if(x)" / "if (x)" 
 Unescape printable chars encoded as \xNN or \uNNNN? 
 Use JSLint-happy formatting tweaks? 
 Indent <head> and <body> sections? 
 Keep indentation on empty lines? 
Use a simple textarea for code input?
Beautify Code (ctrl-enter)
64
    G.Cg = function(b) {
65
        return b instanceof Array ? b : G.Ok(G.Cd(b))
66
    };
67
    G.Vt = function(b, c, d) {
68
        b instanceof String && (b = String(b));
69
        for (var e = b.length, f = 0; f < e; f++) {
70
            var g = b[f];
71
            if (c.call(d, g, f, b)) return {
72
                ym: f,
73
                Jo: g
74
            }
75
        }
76
        return {
77
            ym: -1,
78
            Jo: void 0
79
        }
80
    };
81
    G.zj = !1;
82
    G.cp = !1;
83
    G.ep = !1;
84
    G.Ir = !1;
85
    G.defineProperty = G.zj || typeof Object.defineProperties == t ? Object.defineProperty : function(b, c, d) {
86
        b != Array.prototype && b != Object.prototype && (b[c] = d.value)
87
    };
88
    G.cm = function(b) {
89
        return "undefined" != typeof window && window === b ? b : "undefined" != typeof global && null != global ? global : b
90
    };
91
    G.global = G.cm(this);
92
    G.Ni = function(b, c) {
93
        if (c) {
94
            var d = G.global;
95
            b = b.split(".");
96
            for (var e = 0; e < b.length - 1; e++) {
97
                var f = b[e];
98
                f in d || (d[f] = {});
99
                d = d[f]
100
            }
101
            b = b[b.length - 1];
102
            e = d[b];
103
            c = c(e);
104
            c != e && null != c && G.defineProperty(d, b, {
105
                configurable: !0,
106
                writable: !0,
107
                value: c
108
            })
109
        }
110
    };
111
    G.ql = function(b) {
112
        if (null == b) throw new TypeError("The 'this' value for String.prototype.repeat must not be null or undefined");
113
        return b + ""
114
    };
115
    G.Ni("String.prototype.repeat", function(b) {
116
        return b ? b : function(c) {
117
            var d = G.ql(this);
118
            if (0 > c || 1342177279 < c) throw new RangeError("Invalid count value");
119
            c |= 0;
120
            for (var e = ""; c;)
121
                if (c & 1 && (e += d), c >>>= 1) d += d;
122
            return e
123
        }
124
    });
Beautify Code (ctrl-enter)
Your Selected Options (JSON):

{
  "indent_size": "4",
  "indent_char": " ",
  "max_preserve_newlines": "5",
  "preserve_newlines": true,
  "keep_array_indentation": false,
  "break_chained_methods": false,
  "indent_scripts": "normal",
  "brace_style": "collapse",
  "space_before_conditional": true,
  "unescape_strings": false,
  "jslint_happy": false,
  "end_with_newline": false,
  "wrap_line_length": "0",
  "indent_inner_html": false,
  "comma_first": false,
  "e4x": false,
  "indent_empty_lines": false
}
Not pretty enough for you?  Report a problem with this input

Browser extensions and other uses
A bookmarklet (drag it to your bookmarks) by Ichiro Hiroshi to see all scripts used on the page,
Chrome, in case the built-in CSS and javascript formatting isn't enough for you:
— Quick source viewer by Tomi Mickelsson (github, blog),
— Javascript and CSS Code beautifier by c7sky,
— jsbeautify-for-chrome by Tom Rix (github),
— Pretty Beautiful JavaScript by Will McSweeney
— Stackoverflow Code Beautify by Making Odd Edit Studios (github).
Firefox: Javascript deminifier by Ben Murphy, to be used together with the firebug (github),
Safari: Safari extension by Sandro Padin,
Opera: Readable JavaScript (github) by Dither,
Opera: Source extension by Deathamns,
Sublime Text 2/3: CodeFormatter, a python plugin by Avtandil Kikabidze, supports HTML, CSS, JS and a bunch of other languages,
Sublime Text 2/3: HTMLPrettify, a javascript plugin by Victor Porof,
Sublime Text 2: JsFormat, a javascript formatting plugin for this nice editor by Davis Clark,
vim: sourcebeautify.vim, a plugin by michalliu (requires node.js, V8, SpiderMonkey or cscript js engine),
vim: vim-jsbeautify, a plugin by Maksim Ryzhikov (node.js or V8 required),
Emacs: Web-beautify formatting package by Yasuyuki Oka,
Komodo IDE: Beautify-js addon by Bob de Haas (github),
C#: ghost6991 ported the javascript formatter to C#,
Go: ditashi has ported the javascript formatter to golang,
Beautify plugin (github) by HookyQR for the Visual Studio Code IDE,
Fiddler proxy: JavaScript Formatter addon,
gEdit tips by Fabio Nagao,
Akelpad extension by Infocatcher,
Beautifier in Emacs write-up by Seth Mason,
Cloud9, a lovely IDE running in a browser, working in the node/cloud, uses jsbeautifier (github),
Devenir Hacker App, a non-free JavaScript packer for Mac,
REST Console, a request debugging tool for Chrome, beautifies JSON responses (github),
mitmproxy, a nifty SSL-capable HTTP proxy, provides pretty javascript responses (github).
wakanda, a neat IDE for web and mobile applications has a Beautifier extension (github).
Burp Suite now has a beautfier extension, thanks to Soroush Dalili,
Netbeans jsbeautify plugin by Drew Hamlett (github).
brackets-beautify-extension for Adobe Brackets by Drew Hamlett (github),
codecaddy.net, a collection of webdev-related tools, assembled by Darik Hall,
editey.com, an interesting and free Google-Drive oriented editor uses this beautifier,
a beautifier plugin for Grunt by Vishal Kadam,
SynWrite editor has a JsFormat plugin (rar, readme),
LIVEditor, a live-editing HTML/CSS/JS IDE (commercial, Windows-only) uses the library,
Doing anything interesting? Write us to team@beautifier.io so we can add your project to the list.

Written by Einar Lielmanis, maintained and evolved by Liam Newman.

We use the wonderful CodeMirror syntax highlighting editor, written by Marijn Haverbeke.

Made with a great help of Jason Diamond, Patrick Hof, Nochum Sossonko, Andreas Schneider, 
Dave Vasilevsky, Vital Batmanov, Ron Baldwin, Gabriel Harrison, Chris J. Shull, Mathias Bynens, 
Vittorio Gambaletta, Stefano Sanfilippo and Daniel Stockman.

Run the tests