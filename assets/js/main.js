$(function () {
  "use strict";

  var $date = $(".docs-date");
  var $container = $(".docs-datepicker-container");
  var $trigger = $(".docs-datepicker-trigger");
  var options = {
    show: function (e) {
      console.log(e.type, e.namespace);
    },
    hide: function (e) {
      console.log(e.type, e.namespace);
    },
    pick: function (e) {
      console.log(e.type, e.namespace, e.view);
    },
  };

  $date
    .on({
      "show.datepicker": function (e) {
        console.log(e.type, e.namespace);
      },
      "hide.datepicker": function (e) {
        console.log(e.type, e.namespace);
      },
      "pick.datepicker": function (e) {
        console.log(e.type, e.namespace, e.view);
      },
    })
    .datepicker(options);

  $(".docs-options, .docs-toggles").on("change", function (e) {
    var target = e.target;
    var $target = $(target);
    var name = $target.attr("name");
    var value = target.type === "checkbox" ? target.checked : $target.val();
    var $optionContainer;

    switch (name) {
      case "container":
        if (value) {
          value = $container;
          $container.show();
        } else {
          $container.hide();
        }

        break;

      case "trigger":
        if (value) {
          value = $trigger;
          $trigger.prop("disabled", false);
        } else {
          $trigger.prop("disabled", true);
        }

        break;

      case "inline":
        $optionContainer = $('input[name="container"]');

        if (!$optionContainer.prop("checked")) {
          $optionContainer.click();
        }

        break;

      case "language":
        $('input[name="format"]').val($.fn.datepicker.languages[value].format);
        break;
    }

    options[name] = value;
    $date.datepicker("reset").datepicker("destroy").datepicker(options);
  });

  $(".docs-actions").on("click", "button", function (e) {
    var data = $(this).data();
    var args = data.arguments || [];
    var result;

    e.stopPropagation();

    if (data.method) {
      if (data.source) {
        $date.datepicker(data.method, $(data.source).val());
      } else {
        result = $date.datepicker(data.method, args[0], args[1], args[2]);

        if (result && data.target) {
          $(data.target).val(result);
        }
      }
    }
  });

  $('[data-toggle="datepicker"]').datepicker();
  $('[data-toggle="tooltip"]').tooltip();
});

$(document).ready(function () {
  function readURL(input) {
    if (input.files && input.files[0]) {
      var reader = new FileReader();
      reader.onload = function (e) {
        $("#imagePreview").css(
          "background-image",
          "url(" + e.target.result + ")"
        );
        $("#imagePreview").hide();
        $("#imagePreview").fadeIn(650);
      };
      reader.readAsDataURL(input.files[0]);
    }
  }
  $("#edit-profile").change(function () {
    readURL(this);
  });
});

$(document).on("click", ".Hamburger", function (ev) {
  $(".Sidebar--modifier").toggleClass("toggled");
  $(this).toggleClass("is-active");
});

$(document).on("click", ".close-cart", function (ev) {
  $(".Sidebar--modifier").toggleClass("toggled");
  $(this).toggleClass("is-active");
});

// function increaseValue() {
//   var value = parseInt( el.closest('number').value, 10);
//   value = isNaN(value) ? 0 : value;
//   value++;
//   el.closest('number').value = value;
// }
//  function increaseValue1() {
//   var value = parseInt( el.closest('number1').value, 10);
//   value = isNaN(value) ? 0 : value;
//   value++;
//   el.closest('number1').value = value;
// }

// function decreaseValue() {
// var value = parseInt( el.closest('number').value, 10);
// value = isNaN(value) ? 0 : value;
// value < 1 ? value = 1 : '';
// value--;
// el.closest('number').value = value;
// }
// function decreaseValue1() {
//   var value = parseInt( el.closest('number1').value, 10);
//   value = isNaN(value) ? 0 : value;
//   value < 1 ? value = 1 : '';
//   value--;
//   el.closest('number1').value = value;
// }
$(document).ready(function () {
  // const minus = $('.quantity__minus');
  // const plus = $('.quantity__plus');
  // const input = $('.quantity__input');
  // minus.click(function(e) {
  //   e.preventDefault();
  //   var value = input.val();
  //   if (value > 0) {
  //     value--;
  //   }
  //   input.val(value);
  // });

  // plus.click(function(e) {
  //   e.preventDefault();
  //   var value = input.val();
  //   value++;
  //   input.val(value);
  // })
  $(".quantity__plus").on("click", function (event) {
    var value = $(this).siblings(".quantity__input").val();
    value++;
    $(this).siblings(".quantity__input").val(value);
  });
  $(".quantity__minus").on("click", function (event) {
    var value = $(this).siblings(".quantity__input").val();
    if (value > 0) {
      value--;
    }
    $(this).siblings(".quantity__input").val(value);
  });
  $(".btn-play").on("click", function (event) {
    $(event.target).closest(".cover").children().css({
      "z-index": 5,
      opacity: 1,
      "pointer-events": "all",
    });
    $(event.target).closest(".btn-play").toggleClass("hideBtn");
    // $(event.target).closest('.cover').children(".model-video").children().trigger('play');
    $(event.target)
      .closest(".cover")
      .find(".model-video .video")
      .trigger("play");
  });
});
