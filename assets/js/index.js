// side bar accordian
$('.toggle').click(function(e) {
  	e.preventDefault();
  
    var $this = $(this);
  
    if ($this.next().hasClass('show')) {
        $this.next().removeClass('show');
        $this.next().slideUp(350);
    } else {
        $this.parent().parent().find('li .inner').removeClass('show');
        $this.parent().parent().find('li .inner').slideUp(350);
        $this.next().toggleClass('show');
        $this.next().slideToggle(350);
    }
});
// end side bar accordian

// image upload
$(document).ready(function () {
  $("#btn-upload").click(function () {
    $("#img-name").click();
  });

  $("#img-name").change(function (e) {
    var $imgName = $(this);

    $imgName.next().html($imgName.val().split('\\').pop());
  });
});
// end image upload
